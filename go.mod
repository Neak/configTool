module configTool

go 1.12

require (
	github.com/360EntSecGroup-Skylar/excelize v1.4.1
	github.com/json-iterator/go v1.1.9
)
