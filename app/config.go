/**
 *	Author: Neak
 *	Date: 2020/4/9
 *  Desc:
 *	extra:
 */
package app

import (
	"fmt"
	jsoniter "github.com/json-iterator/go"
	"io/ioutil"
)

type ConfigInfo struct {
	ConfigPath   string `json: "configPath"`   // 配置表文件夹路径
	TemplatePath string `json: "templatePath"` // 模板文件夹路径

	GenPath   string `json:"genPath"`      // 生成文件夹路径

	GoApiName string `json:"GoApiName"` // 配置文件处理工具文件名，不填则没有
	GoApiImport string `json:"GoApiImport"` // Golang api引用的包名
	GoApiPackage string `json:"GoApiPackage"` // Golang Api包名

	devLangPkg  string // 开发语言的包名
	GoPackage   string `json:"GoPackage"`   // Golang配置包名
	JavaPackage string `json:"JavaPackage"` // Java配置包名
}

// 加载基本配置 config.json
func loadConfig() *ConfigInfo {
	content, err := ioutil.ReadFile("config.json")
	if err != nil {
		panic(fmt.Sprintf("错误 err：%v", err))
	}
	var conf ConfigInfo
	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	err = json.Unmarshal(content, &conf)
	if err != nil {
		panic(fmt.Sprintf("错误 err：%v", err))
	}
	jsonText, err := PrettyJson(conf)
	if err != nil {
		fmt.Println("config.json: \n", jsonText)
	}

	// api引用包名组成为：包路径/api包名/配置包名
	conf.GoApiImport = fmt.Sprintf("%v/%v/%v", conf.GoApiImport, conf.GoApiPackage, conf.GoPackage)

	return &conf
}

// 设置包名
func (c *ConfigInfo) setPackage(pkg string) {
	c.devLangPkg = pkg
}


// 输出美观json字符串
func PrettyJson(data interface{}) (string, error) {
	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	if body, err := json.MarshalIndent(data, "", "  "); err != nil {
		return "", err
	} else {
		return string(body), nil
	}
}
