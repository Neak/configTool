// Author：NeakHuang
// CreateDate: 2020/4/9
// Modifier：
// ModDate：
// Desc: 生成golang的实体文件
package app

import (
	"bytes"
	"configTool/utils"
	"fmt"
	"path/filepath"
	"strings"
	"text/template"
)

type ExcelToGo struct {
	*AbstractExcelTo
}

func NewExcelToGo(lang, exportFile, csSign string) *ExcelToGo {
	var excelTo = &ExcelToGo{
		NewAExcelTo(lang, exportFile, csSign),
	}
	excelTo.initReal(excelTo, Golang)
	return excelTo
}

// 开始处理生成
// 子类重写
func (e *ExcelToGo) startGen(entityName, lang string) (success bool) {
	utils.DirExists(e.genDir)

	if Config.devLangPkg == "" {
		LogError("× golang包名没有设置")
		return
	}

	// 服务端数据
	if strings.Contains(e.csSign, "s") {
		// 生成json文件
		if success = e.genJsonFile(e.serverResultList, entityName); !success {
			return
		}

		// 构建生成模板的实例对象
		var iEntityTpl = e.buildEntityTemplate(entityName, lang, GenServer)
		if iEntityTpl == nil {
			return
		}
		var entityTpl = iEntityTpl.(*EntityTemplate)
		// 生成实体
		if success = e.genEntity(entityTpl); !success {
			return
		}
		Log("√ 成功生成 golang 服务端实例 %v ", entityName)
	}

	// 客户端数据
	if strings.Contains(e.csSign, "c") {
		// 生成json文件
		if success = e.genJsonFile(e.clientResultList, entityName); !success {
			return
		}

		// 构建生成模板的实例对象
		var iEntityTpl = e.buildEntityTemplate(entityName, lang, GenClient)
		if iEntityTpl == nil {
			return
		}
		var entityTpl = iEntityTpl.(*EntityTemplate)
		// 生成实体
		if success = e.genEntity(entityTpl); !success {
			return
		}
		Log("√ 成功生成 golang 客户端实例 %v ", entityName)
	}

	success = true
	return
}

// 开始生成实体
func (e *ExcelToGo) genEntity(entityTpl *EntityTemplate) (success bool) {
	// 开始解析模板
	var tpl = template.New("ConfigEntity")
	var entityTplText = utils.GetTemplate(fmt.Sprintf("%v/%v.config_entity.tplt.txt", Config.TemplatePath, Golang))
	var _, err = tpl.Parse(entityTplText)
	if err != nil {
		LogError("× 忽略实例：解析模板错误 < %v > \nerr：%v", entityTpl.EntityName, err)
		return
	}

	var buf bytes.Buffer
	// FIXME 模板需要修改成go.template的格式
	err = tpl.Execute(&buf, entityTpl)
	if err != nil {
		LogError("× 处理实例模板失败 < %v > \nerr：%v", entityTpl.EntityName, err)
		return
	}

	code, err := utils.BeautifulFormatByte(buf)
	if err != nil {
		LogError("× 生成实例失败，格式美化错误 < %v > \nerr：%v", entityTpl.EntityName, err)
		return
	}
	// golang的文件名做特殊处理
	var fileName = fmt.Sprintf("%v.go", entityTpl.EntityName)
	fileName = fmt.Sprintf("%v", utils.ToGolangFileName(fileName, ' '))
	var genDir = filepath.Join(e.genDir, Config.GoPackage)
	// 判断文件夹是否存在
	utils.DirExists(genDir)
	WriteEntityFile(genDir, fileName, code)
	success = true
	return
}

// 生成工具类
func (e *ExcelToGo) genApiConfig() {
	// 开始解析模板
	var tpl = template.New("ConfigTool")
	var entityTplText = utils.GetTemplate(fmt.Sprintf("%v/%v.api.tplt.txt", Config.TemplatePath, Golang))
	var _, err = tpl.Parse(entityTplText)
	if err != nil {
		LogError("× 生成接口文件，解析模板错误 err：%v", err)
		return
	}

	var buf bytes.Buffer
	err = tpl.Execute(&buf, e.entityApi)
	if err != nil {
		LogError("× 处理接口文件模板失败 err：%v", err)
		return
	}
	code, err := utils.BeautifulFormatByte(buf)
	if err != nil {
		LogError("× 生成接口文件失败，格式美化错误 err：%v", err)
	}
	var fileName = fmt.Sprintf("%v.go", Config.GoApiName)
	WriteEntityFile(e.genDir, fileName, code)
}

// 获取对应开发语言中的实际类型
// 子类重写
func (e *ExcelToGo) getSnType(factFieldType string) (snType string) {
	snType = factFieldType
	return
}

// 获取对应开发语言中的实际类型
// 子类重写
func (e *ExcelToGo) toDevLangType(fieldType string) (factType string) {
	var fieldTypeLen = len(fieldType)
	var isArray = fieldType[fieldTypeLen-2:fieldTypeLen] == "[]"
	// 判断是否是数组
	if isArray {
		fieldType = fieldType[0 : fieldTypeLen-2]
	}

	switch fieldType {
	case TypeBool, TypeBoolean:
		factType = "bool"
	case TypeInt, TypeInt32:
		factType = "int32"
	case TypeLong, TypeInt64:
		factType = "int64"
	case TypeFloat, TypeFloat32:
		factType = "float32"
	case TypeDouble, TypeFloat64:
		factType = "float64"
	case Typestring, TypeString, TypeLocString:
		factType = "string"
	default:
		//LogError("× toDevLangType-错误的字段类型：%v", factFieldType)
	}
	// 数组， 拼接成[]xxx
	if isArray {
		factType = fmt.Sprintf("[]%v", factType)
	}
	return
}

// 获取实际值类型
// golang 逗号分割，转成数组
func (e *ExcelToGo) processValByType(cellVal string, fieldType string) (factVal interface{}) {
	fieldType = strings.ToLower(fieldType)
	//if factFieldType == "" || len(factFieldType) <= 0 {
	//	return
	//}

	switch fieldType {
	case TypeBool, TypeBoolean:
		factVal = utils.BoolValue(cellVal)
	case TypeInt, TypeInt32:
		factVal = utils.Int32Value(cellVal)
	case TypeLong, TypeInt64:
		factVal = utils.Int64Value(cellVal)
	case TypeFloat, TypeFloat32:
		factVal = utils.Float32Value(cellVal)
	case TypeDouble, TypeFloat64:
		factVal = utils.Float64Value(cellVal)
	case Typestring, TypeString, TypeLocString:
		factVal = cellVal
	case TypeBoolArray, TypeBooleanArray:
		factVal = utils.StringToBoolSlice(cellVal)
	case TypeIntArray, TypeInt32Array:
		factVal = utils.StringToInt32Slice(cellVal)
	case TypeLongArray, TypeInt64Array:
		factVal = utils.StringToInt64Slice(cellVal)
	case TypeFloatArray, TypeFloat32Array:
		factVal = utils.StringToFloat32Slice(cellVal)
	case TypeDoubleArray, TypeFloat64Array:
		factVal = utils.StringToFloat64Slice(cellVal)
	case TypestringArray, TypeStringArray, TypeLocStringArray:
		factVal = utils.StringToStrSlice(cellVal)
	default:
		LogError("× processValByType-错误的字段类型：%v", fieldType)
	}
	return
}

// 插入值：<FieldName, val>
func (e *ExcelToGo) putFieldValueInMap(valMap map[string]interface{}, factFieldVal interface{}, fieldName string) {

}
