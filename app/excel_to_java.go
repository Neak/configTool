// Author：NeakHuang
// CreateDate: 2020/5/6
// Modifier：
// ModDate：
// Desc: 生成java的实体文件
package app

import (
	"bytes"
	"configTool/utils"
	"fmt"
	"strings"
	"text/template"
)

type ExcelToJava struct {
	*AbstractExcelTo
}

func NewExcelToJava(lang, exportFile, csSign string) *ExcelToJava {
	var excelTo = &ExcelToJava{
		NewAExcelTo(lang, exportFile, csSign),
	}
	excelTo.initReal(excelTo, Java)
	return excelTo
}

// 开始处理生成
// 子类重写
func (e *ExcelToJava) startGen(entityName, lang string) (success bool) {
	utils.DirExists(e.genDir)

	if Config.devLangPkg == "" {
		LogError("× Java包名没有设置")
		return
	}

	// 服务端数据
	if strings.Contains(e.csSign, "s") {
		// 生成json文件
		if success = e.genJsonFile(e.serverResultList, entityName); !success {
			return
		}

		// 构建生成模板的实例对象
		var iEntityTpl = e.buildEntityTemplate(entityName, lang, GenServer)
		if iEntityTpl == nil {
			return
		}
		var entityTpl = iEntityTpl.(*JavaEntityTemplate)
		// 生成实体
		if success = e.genEntity(entityTpl); !success {
			return
		}
		Log("√ 成功生成 Java 服务端实例 %v ", entityName)
	}

	// 客户端数据
	if strings.Contains(e.csSign, "c") {
		// 生成json文件
		if success = e.genJsonFile(e.clientResultList, entityName); !success {
			return
		}

		// 构建生成模板的实例对象
		var iEntityTpl = e.buildEntityTemplate(entityName, lang, GenClient)
		if iEntityTpl == nil {
			return
		}
		var entityTpl = iEntityTpl.(*JavaEntityTemplate)
		// 生成实体
		if success = e.genEntity(entityTpl); !success {
			return
		}
		Log("√ 成功生成 Java 客户端实例 %v ", entityName)
	}

	success = true
	return
}

func (e *ExcelToJava) buildEntityTemplate(entityName, lang string, genFlag string) interface{} {
	var iEntityTpl = e.AbstractExcelTo.buildEntityTemplate(entityName, lang, genFlag)
	if iEntityTpl == nil {
		return nil
	}
	var entityTpl = iEntityTpl.(*EntityTemplate)

	// 构造函数参数string
	var ctorParams = ""
	// 初始化函数参数string
	var initParams = ""
	for i, entityField := range entityTpl.EntityFieldList {
		var ctorParamStr, initParamStr = e._buildInitParams(entityField)
		var recordNum = i + 1
		ctorParams += ctorParamStr
		initParams += initParamStr
		if recordNum != len(entityTpl.EntityFieldList) {
			ctorParams += ", "
			initParams += ", "
		}
		if recordNum%3 == 0 && recordNum != len(entityTpl.EntityFieldList) {
			initParams += "\n\t\t\t\t\t"
		}
	}

	var javaEntityTpl = &JavaEntityTemplate{
		EntityTemplate: entityTpl,
		CtorParamStr:   ctorParams,
		InitParamStr:   initParams,
	}

	return javaEntityTpl
}

// 开始生成实体
func (e *ExcelToJava) genEntity(iEntityTpl interface{}) (success bool) {
	var entityTpl = iEntityTpl.(*JavaEntityTemplate)
	// 开始解析模板
	var tpl = template.New("ConfigEntity")
	var entityTplText = utils.GetTemplate(fmt.Sprintf("%v/%v.config_entity.tplt.txt", Config.TemplatePath, Java))
	var _, err = tpl.Parse(entityTplText)
	if err != nil {
		LogError("× 忽略实例：解析模板错误 < %v > \nerr：%v", entityTpl.EntityName, err)
		return
	}

	var buf bytes.Buffer
	// FIXME 模板需要修改成go.template的格式
	err = tpl.Execute(&buf, entityTpl)
	if err != nil {
		LogError("× 处理实例模板失败 < %v > \nerr：%v", entityTpl.EntityName, err)
		return
	}

	// Java的文件名做特殊处理
	var fileName = fmt.Sprintf("%v.java", entityTpl.EntityName)
	WriteEntityFile(e.genDir, fileName, buf.Bytes())
	success = true
	return
}

// 获取对应开发语言中的实际类型
// 子类重写
func (e *ExcelToJava) getSnType(factFieldType string) (snType string) {
	switch factFieldType {
	case TypeInt:
		snType = "Integer"
	case TypeLong:
		snType = "Long"
	case TypeFloat:
		snType = "Float"
	case TypeDouble, TypeFloat64:
		snType = "Double"
	case Typestring, TypeString, TypeLocString:
		snType = "String"
	}
	return
}

// 获取对应开发语言中的实际类型
// 子类重写
func (e *ExcelToJava) toDevLangType(fieldType string) (factType string) {
	var fieldTypeLen = len(fieldType)
	var isArray = fieldType[fieldTypeLen-2:fieldTypeLen] == "[]"
	// 判断是否是数组
	if isArray {
		fieldType = fieldType[0 : fieldTypeLen-2]
	}

	switch fieldType {
	case TypeBool, TypeBoolean:
		factType = "boolean"
	case TypeInt, TypeInt32:
		factType = "int"
	case TypeLong, TypeInt64:
		factType = "long"
	case TypeFloat, TypeFloat32:
		factType = "float"
	case TypeDouble, TypeFloat64:
		factType = "double"
	case Typestring, TypeString, TypeLocString:
		factType = "String"
	default:
		//LogError("× toDevLangType-错误的字段类型：%v", factFieldType)
	}
	// 数组， 拼接成xxx[]
	if isArray {
		factType = fmt.Sprintf("%v[]", factType)
	}
	return
}

// 获取实际值类型
// java没有将逗号分割的string转成对应的数组
func (e *ExcelToJava) processValByType(cellVal string, fieldType string) (factVal interface{}) {
	fieldType = strings.ToLower(fieldType)
	//if factFieldType == "" || len(factFieldType) <= 0 {
	//	return
	//}

	switch fieldType {
	case TypeBool, TypeBoolean:
		factVal = utils.BoolValue(cellVal)
	case TypeInt, TypeInt32:
		factVal = utils.Int32Value(cellVal)
	case TypeLong, TypeInt64:
		factVal = utils.Int64Value(cellVal)
	case TypeFloat, TypeFloat32:
		factVal = utils.Float32Value(cellVal)
	case TypeDouble, TypeFloat64:
		factVal = utils.Float64Value(cellVal)
	default:
		factVal = cellVal
	}
	return
}

// 获取初始化参数的字符串
func (e *ExcelToJava) _buildInitParams(entityField *EntityField) (ctorParam string, initParam string) {
	// 映射到json中的表原字段名称
	var filedOriName = entityField.FieldOriName
	ctorParam = fmt.Sprintf("%v %v", entityField.FieldType, filedOriName)

	var fieldType = entityField.FieldType
	switch fieldType {
	case TypeBool, TypeBoolean:
		initParam = fmt.Sprintf(`conf.getBooleanValue("%v")`, filedOriName)
	case TypeInt, TypeInt32:
		initParam = fmt.Sprintf(`conf.getIntValue("%v")`, filedOriName)
	case TypeLong, TypeInt64:
		initParam = fmt.Sprintf(`conf.getLongValue("%v")`, filedOriName)
	case TypeFloat, TypeFloat32:
		initParam = fmt.Sprintf(`conf.getFloatValue("%v")`, filedOriName)
	case TypeDouble, TypeFloat64:
		initParam = fmt.Sprintf(`conf.getDoubleValue("%v")`, filedOriName)
	case TypeBoolArray, TypeBooleanArray:
		initParam = fmt.Sprintf(`parseBoolArray(conf.getString("%v"))`, filedOriName)
	case TypeIntArray, TypeInt32Array:
		initParam = fmt.Sprintf(`parseIntArray(conf.getString("%v"))`, filedOriName)
	case TypeLongArray, TypeInt64Array:
		initParam = fmt.Sprintf(`parseLongArray(conf.getString("%v"))`, filedOriName)
	case TypeFloatArray, TypeFloat32Array:
		initParam = fmt.Sprintf(`parseFloatArray(conf.getString("%v"))`, filedOriName)
	case TypeDoubleArray, TypeFloat64Array:
		initParam = fmt.Sprintf(`parseDoubleArray(conf.getString("%v"))`, filedOriName)
	case TypestringArray, TypeStringArray, TypeLocStringArray:
		initParam = fmt.Sprintf(`parseStringArray(conf.getString("%v"))`, filedOriName)
	default:
		initParam = fmt.Sprintf(`conf.getString("%v")`, filedOriName)
	}

	return
}
