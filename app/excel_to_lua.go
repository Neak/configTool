// Author：NeakHuang
// CreateDate: 2020/5/19
// Modifier：
// ModDate：
// Desc: 生成lua配置，由于lua语法特性，特殊处理
// -----------------------------------------------
// Lua Table 通常被用来存储游戏的配置数据，如果配置中有很多冗余重复的数据那么将占用较多的内存，严重影响加载速度
// __rt：配置中的数组片段，全都存在这里
// data：存储所有配置
// __default_values:获取字段中使用最多次数的值作为默认值，并且删除默认值字段
// 使用了非ASCII字符集字符的字段被视为需要做多语言化处理并提取替换成特殊标识符
// 唯一化所有的子table，并且指向同一个引用，以节约内存
// 思路参考：https://github.com/lujian101/LuaTableOptimizer
// -----------------------------------------------
package app

import (
	"bytes"
	"configTool/utils"
	"fmt"
	"sort"
	"strings"
	"text/template"
)

type luaData struct {
	// 统计重复的字段值<field, []*luaFieldStats>
	fieldStatsMap map[string]luaFieldStatsList

	rtList []string // 用于存放所有的数组片段
	// <aryData, index>
	rtMap    map[string]int // 数组为key，下标为val
}

func NewLuaData() *luaData {
	return &luaData{
		fieldStatsMap: make(map[string]luaFieldStatsList),
		rtList:        make([]string, 0),
		rtMap:         make(map[string]int),
	}
}

// lua的字段详细
type luaFieldStats struct {
	fieldName     string
	factFieldType string
	cellVal       string // 原始表数据
	val           interface{}
	count         int
}

// 获取组合名称， fieldName=val
func (f *luaFieldStats) dataFmt() (dataFmt string) {
	return _luaDataFmt(f.factFieldType, f.fieldName, f.val)
}

// 排序接口
type luaFieldStatsList []*luaFieldStats

func (s luaFieldStatsList) Len() int           { return len(s) }
func (s luaFieldStatsList) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
func (s luaFieldStatsList) Less(i, j int) bool { return s[i].count > s[j].count }

type ExcelToLua struct {
	*AbstractExcelTo

	serverLuaData *luaData
	clientLuaData *luaData

	// TODO 对比测试用，用于存储所有源数据
	sNormalResultList []map[string]interface{}
	cNormalResultList []map[string]interface{}
}

func NewExcelToLua(lang, exportFile, csSign string) *ExcelToLua {
	var excelTo = &ExcelToLua{
		AbstractExcelTo:   NewAExcelTo(lang, exportFile, csSign),
		sNormalResultList: make([]map[string]interface{}, 0),
		cNormalResultList: make([]map[string]interface{}, 0),
	}
	excelTo.initReal(excelTo, Lua)
	return excelTo
}

// 处理单个数据表 sheet
// rows：所有行数据
func (e *ExcelToLua) processSheet(rows [][]string) (success bool) {
	e.sFieldNames = make([]string, 0)
	e.cFieldNames = make([]string, 0)

	e.serverLuaData = NewLuaData()
	e.clientLuaData = NewLuaData()

	for rowIdx, row := range rows {
		// 服务端数据
		var serverMap = make(map[string]interface{})
		// 客户端数据
		var clientMap = make(map[string]interface{})

		// 双端不优化lua数据
		var sNormalMap = make(map[string]interface{})
		var cNormalMap = make(map[string]interface{})

		for colIdx, cellVal := range row {
			var csFlag = e.getFieldCS(rows, colIdx) // 转成小写
			// 第1行的时候，头信息存储好（处理前四行数据）
			if rowIdx == FlagLineCS {
				// 忽略没有定义cs的列
				if len(csFlag) <= 0 {
					continue
				}
				e._saveHeadInfo(rows, colIdx, csFlag)
			}
			// 前4行，定义头信息
			if rowIdx <= FlagLineFieldDesc {
				continue
			}

			// 字段类型
			var fieldType, factFieldType, fieldName, _ = e.getFieldInfo(rows, colIdx)
			if factFieldType == "" || fieldName == "" {
				continue
			}

			// 检查第一列sn是否存在，不存在则忽略此行，并且抛出异常日志
			if colIdx == 0 {
				// sn统一为小写
				fieldName = strings.ToLower(fieldName)
				// 检查sn是否存在
				e.checkSnAndSaveType(cellVal, factFieldType, rowIdx)
			}

			var fieldStats = &luaFieldStats{
				fieldName:     fieldName,
				factFieldType: factFieldType,
				cellVal:       cellVal,
			}

			// 获取数据实际类型的值 interface{}
			var factFieldVal = e.processValByType(cellVal, fieldType)
			// 是数组，则把真实数据转成
			if strings.Contains(factFieldType, "[]") {
				// 转成luaTable
				factFieldVal = e._aryData2LuaTableString(factFieldType, cellVal, factFieldVal)
			}

			var fieldStatsMap map[string]luaFieldStatsList
			// 服务端数据
			if strings.Contains(csFlag, "s") && strings.Contains(e.csSign, "s") {

				sNormalMap[fieldName] = factFieldVal
				// 服务端字段统计map
				fieldStatsMap = e.serverLuaData.fieldStatsMap // 统计map

				fieldStats.val = factFieldVal
				// 判断是否是数组
				if index, ok := e.processVal2RT(factFieldType, factFieldVal, e.serverLuaData); ok {
					// 数组则存放数组里的index
					fieldStats.val = fmt.Sprintf("__rt[%v]", index)
				}
				serverMap[fieldName] = fieldStats.val
			}

			// 客户端数据
			if strings.Contains(csFlag, "c") && strings.Contains(e.csSign, "c") {
				cNormalMap[fieldName] = factFieldVal
				// 客户端字段统计map
				fieldStatsMap = e.clientLuaData.fieldStatsMap // 统计map

				fieldStats.val = factFieldVal
				// 判断是否是数组
				if index, ok := e.processVal2RT(factFieldType, factFieldVal, e.clientLuaData); ok {
					// 数组则存放数组里的index
					fieldStats.val = fmt.Sprintf("__rt[%v]", index)
				}
				clientMap[fieldName] = fieldStats.val
			}

			// 如果统计map为空，则这是一行不需要记录的数据
			if fieldStatsMap == nil {
				continue
			}
			// 数据统计
			var fieldStatsList = fieldStatsMap[fieldName]
			if fieldStatsList == nil {
				fieldStats.count = 1
				fieldStatsList = append(fieldStatsList, fieldStats)
				fieldStatsMap[fieldName] = fieldStatsList
			} else {
				// 如果存在，则遍历相同字段，看看是否有重复的值，有的话，则计数+1
				var sameVal = false
				for _, v := range fieldStatsList {
					if v.cellVal == cellVal {
						sameVal = true
						v.count += 1
						break
					}
				}
				if !sameVal {
					fieldStatsList = append(fieldStatsList, fieldStats)
					fieldStatsMap[fieldName] = fieldStatsList
				}
			}
		}

		if len(serverMap) > 0 {
			e.serverResultList = append(e.serverResultList, serverMap)
			e.sNormalResultList = append(e.sNormalResultList, sNormalMap)
		}

		if len(clientMap) > 0 {
			e.clientResultList = append(e.clientResultList, clientMap)
			e.cNormalResultList = append(e.cNormalResultList, cNormalMap)
		}
	}

	return true
}

// 处理数据 是否归属于RT
// factFieldType：数据类型
// cellVal：表中的数据
// factFieldVal：转换后的数据
// rtList：存储数组的列表
// rtMap：存储数组中数据对应的map列表
// return
//		index：RT下标
//		ok：是否是数组
func (e *ExcelToLua) processVal2RT(factFieldType string, factFieldVal interface{}, luaData *luaData) (index int, ok bool) {
	// 非数组
	if !strings.Contains(factFieldType, "[]") {
		return
	}
	// 转成luaTable
	var luaAryData = factFieldVal.(string)

	// 判断list是否存在
	if luaData.rtMap[luaAryData] > 0 {
		return luaData.rtMap[luaAryData], true
	}
	// 数组加入rtList中
	luaData.rtList = append(luaData.rtList, luaAryData)

	// 没有减1，是因为lua的index从1开始
	luaData.rtMap[luaAryData] = len(luaData.rtList)

	return len(luaData.rtList), true
}

// 开始处理生成
// 子类重写
func (e *ExcelToLua) startGen(entityName, lang string) (success bool) {
	utils.DirExists(e.genDir)

	// 生成json文件
	Log("√ Lua无需生成json文件")

	// 服务端数据
	if strings.Contains(e.csSign, "s") {
		// 构建生成模板的实例对象
		var iEntityTpl = e.buildEntityTemplate(entityName, lang, GenServer)
		if iEntityTpl == nil {
			return
		}
		// 生成实体
		if success = e.genEntity(iEntityTpl); !success {
			return
		}
		Log("√ 成功生成 Lua 服务端实例 %v ", entityName)
	}

	// 客户端数据
	if strings.Contains(e.csSign, "c") {
		// 构建生成模板的实例对象
		var iEntityTpl = e.buildEntityTemplate(entityName, lang, GenClient)
		if iEntityTpl == nil {
			return
		}
		// 生成实体
		if success = e.genEntity(iEntityTpl); !success {
			return
		}
		Log("√ 成功生成 Lua 客户端实例 %v ", entityName)
	}

	success = true
	return
}

// 构建lua的模板实体
func (e *ExcelToLua) buildEntityTemplate(entityName, lang string, genFlag string) interface{} {
	var iEntityTpl = e.AbstractExcelTo.buildEntityTemplate(entityName, lang, genFlag)
	if iEntityTpl == nil {
		return nil
	}

	var entityTpl = iEntityTpl.(*EntityTemplate)
	var luaData *luaData
	// 实体对象对象，用于字段有序化
	var entityFieldList []*EntityField
	// 结果数据
	var resultList []map[string]interface{}
	// 原值数据
	var normalList []map[string]interface{}

	if genFlag == GenServer {
		luaData = e.serverLuaData
		entityFieldList = e.serverEntityFieldList
		resultList = e.serverResultList
		normalList = e.sNormalResultList
	} else if genFlag == GenClient {
		luaData = e.clientLuaData
		entityFieldList = e.clientEntityFieldList
		resultList = e.clientResultList
		normalList = e.cNormalResultList
	} else {
		LogError("× 忽略实例，生成端标识错误 %v , genFlag(c|s)：%v", entityName, genFlag)
		return nil
	}

	// 重复率最高的数值<fieldName, val>
	var repeMaxMap = make(map[string]interface{})
	// 重复率最高的配置 {sn=1, param1="param1",...}
	var defaultList = make([]string, 0)

	// 遍历所有字段的值，取出重复率最高的那个
	var fieldStatsList luaFieldStatsList
	for _, entityField := range entityFieldList {
		fieldStatsList = luaData.fieldStatsMap[entityField.FieldOriName]
		// 降序排序
		sort.Sort(fieldStatsList)

		// 该字段重复率最高的值为下标为0的值
		repeMaxMap[entityField.FieldOriName] = fieldStatsList[0].val

		var repeMaxDataFmt = fieldStatsList[0].dataFmt()
		defaultList = append(defaultList, repeMaxDataFmt)
	}

	// 遍历结果表，去重
	var dataList []string       // 存储所有优化后的配置
	for i, factDataMap := range resultList {
		var factVal interface{}
		var data string
		// 遍历行数据中的所有字段
		for _, entityField := range entityFieldList {
			factVal = factDataMap[entityField.FieldOriName]
			if factVal == repeMaxMap[entityField.FieldOriName] {
				continue
			}
			factVal = factDataMap[entityField.FieldOriName]
			data += _luaDataFmt(entityField.FieldType, entityField.FieldOriName, factVal) + ","
		}
		if len(data) <= 0 {
			continue
		}
		// 最后一个逗号删除
		data = data[:len(data)-1]
		if i < len(resultList)-1 {
			data = fmt.Sprintf("{%v},", data)
		} else {
			data = fmt.Sprintf("{%v}", data)
		}
		data = strings.Replace(data, "\n", "\\n", -1)
		dataList = append(dataList, data)
	}

	// TODO 对比测试
	// 遍历结果表，原始数据
	var normalDataList = make([]string, 0)
	for i, factDataMap := range normalList {
		var factVal interface{}
		var data string
		// 遍历行数据中的所有字段
		for _, entityField := range entityFieldList {
			factVal = factDataMap[entityField.FieldOriName]
			data += _luaDataFmt(entityField.FieldType, entityField.FieldOriName, factVal) + ","
		}
		if len(data) <= 0 {
			continue
		}
		// 最后一个逗号删除
		data = data[:len(data)-1]
		if i < len(resultList)-1 {
			data = fmt.Sprintf("{%v},", data)
		} else {
			data = fmt.Sprintf("{%v}", data)
		}
		data = strings.Replace(data, "\n", "\\n", -1)
		normalDataList = append(normalDataList, data)
	}
	////////////////////////////////

	var luaEntityTpl = &LuaEntityTemplate{
		EntityTemplate: entityTpl,
		RTList:         luaData.rtList,
		DataList:       dataList,
		DefaultList:    defaultList,
		NormalList:     normalDataList,

	}

	return luaEntityTpl
}

// 开始生成实体
func (e *ExcelToLua) genEntity(iEntityTpl interface{}) (success bool) {
	var entityTpl = iEntityTpl.(*LuaEntityTemplate)
	// 开始解析模板
	var tpl = template.New("ConfigEntity")
	var entityTplText = utils.GetTemplate(fmt.Sprintf("%v/%v.config_entity.tplt.txt", Config.TemplatePath, Lua))
	var _, err = tpl.Parse(entityTplText)
	if err != nil {
		LogError("× 忽略实例：解析模板错误 < %v > \nerr：%v", entityTpl.EntityName, err)
		return
	}

	var buf bytes.Buffer
	err = tpl.Execute(&buf, entityTpl)
	if err != nil {
		LogError("× 处理实例模板失败 < %v > \nerr：%v", entityTpl.EntityName, err)
		return
	}

	// Lua的文件名做特殊处理
	var fileName = fmt.Sprintf("%v.lua", entityTpl.EntityName)
	WriteEntityFile(e.genDir, fileName, buf.Bytes())

	// TODO 对比测试
	e.genNormalEntity(iEntityTpl)
	success = true
	return
}

// 开始生成实体
func (e *ExcelToLua) genNormalEntity(iEntityTpl interface{}) (success bool) {
	var entityTpl = iEntityTpl.(*LuaEntityTemplate)
	// 开始解析模板
	var tpl = template.New("ConfigEntity")
	var entityTplText = utils.GetTemplate(fmt.Sprintf("%v/%v.config_nor_entity.tplt.txt", Config.TemplatePath, Lua))
	var _, err = tpl.Parse(entityTplText)
	if err != nil {
		LogError("× 忽略实例：解析模板错误 < %v > \nerr：%v", entityTpl.EntityName, err)
		return
	}

	var buf bytes.Buffer
	err = tpl.Execute(&buf, entityTpl)
	if err != nil {
		LogError("× 处理实例模板失败 < %v > \nerr：%v", entityTpl.EntityName, err)
		return
	}

	// Lua的文件名做特殊处理
	var fileName = fmt.Sprintf("%v.nor.lua", entityTpl.EntityName)
	WriteEntityFile(e.genDir, fileName, buf.Bytes())
	success = true
	return
}

// 清除容器
func (e *ExcelToLua) cleanContainer() {
	e.AbstractExcelTo.cleanContainer()
	// 清除已写入的容器
	e.cNormalResultList = make([]map[string]interface{}, 0)
	e.sNormalResultList = make([]map[string]interface{}, 0)
}

// 获取对应开发语言中的实际类型
// 子类重写
func (e *ExcelToLua) toDevLangType(fieldType string) (factType string) {
	var fieldTypeLen = len(fieldType)
	var isArray = fieldType[fieldTypeLen-2:fieldTypeLen] == "[]"
	// 判断是否是数组
	if isArray {
		fieldType = fieldType[0 : fieldTypeLen-2]
	}

	switch fieldType {
	case TypeBool, TypeBoolean:
		factType = "bool"
	case TypeInt, TypeInt32:
		factType = "int32"
	case TypeLong, TypeInt64:
		factType = "int64"
	case TypeFloat, TypeFloat32:
		factType = "float32"
	case TypeDouble, TypeFloat64:
		factType = "float64"
	case Typestring, TypeString, TypeLocString:
		factType = "string"
	default:
		//LogError("× toDevLangType-错误的字段类型：%v", factFieldType)
	}
	// 数组， 拼接成xxx[]
	if isArray {
		factType = fmt.Sprintf("%v[]", factType)
	}
	return
}

// 获取实际值类型
// Lua 逗号分割，转成数组
func (e *ExcelToLua) processValByType(cellVal string, fieldType string) (factVal interface{}) {
	fieldType = strings.ToLower(fieldType)
	//if factFieldType == "" || len(factFieldType) <= 0 {
	//	return
	//}

	switch fieldType {
	case TypeBool, TypeBoolean:
		factVal = utils.BoolValue(cellVal)
	case TypeInt, TypeInt32:
		factVal = utils.Int32Value(cellVal)
	case TypeLong, TypeInt64:
		factVal = utils.Int64Value(cellVal)
	case TypeFloat, TypeFloat32:
		factVal = utils.Float32Value(cellVal)
	case TypeDouble, TypeFloat64:
		factVal = utils.Float64Value(cellVal)
	case Typestring, TypeString, TypeLocString:
		factVal = cellVal
	case TypeBoolArray, TypeBooleanArray:
		fallthrough
	case TypeIntArray, TypeInt32Array:
		fallthrough
	case TypeLongArray, TypeInt64Array:
		fallthrough
	case TypeFloatArray, TypeFloat32Array:
		fallthrough
	case TypeDoubleArray, TypeFloat64Array:
		fallthrough
	case TypestringArray, TypeStringArray, TypeLocStringArray:
		factVal = utils.StringToStrSlice(cellVal)
	default:
		LogError("× processValByType-错误的字段类型：%v", fieldType)
	}
	return
}

// 将数组数据转成lua表字符串
// eg：
//		int[]{1,2,3,4} => {1,2,3,4}
// 		string[]{"a", "b", "c", "d"} = {"a", "b", "c", "d"}
func (e *ExcelToLua) _aryData2LuaTableString(factFieldType, cellVal string, factFieldVal interface{}) (luaAryData string) {
	luaAryData = fmt.Sprintf("{%v}", cellVal)
	if cellVal != "" && strings.Contains(factFieldType, "string") {
		luaAryData = fmt.Sprintf("{\"%v\"}", strings.Join(factFieldVal.([]string), `", "`))
		//} else {
		//	var temp = make([]string, len(factVal))
		//	for k, v := range factVal {
		//		temp[k] = fmt.Sprintf("%d", v)
		//	}
		//	luaAryData = fmt.Sprintf("{\"%v\"}", strings.Join(temp, `","`))
	}
	return
}

func _luaDataFmt(factFieldType, fieldName string, fieldVal interface{}) (dataFmt string) {
	// string，但是不是数组
	if strings.Contains(factFieldType, "string") && !strings.Contains(factFieldType, "[]"){
		dataFmt = `%v="%v"` // string：fieldName="val"
	} else {
		dataFmt = "%v=%v" // other： fieldName=val
	}
	dataFmt = fmt.Sprintf(dataFmt, fieldName, fieldVal)
	return
}
