/**
 *	Author: Neak
 *	Date: 2020/4/9
 *  Desc:
 *	extra:
 */
package app

import (
	"configTool/utils"
	"flag"
	"fmt"
	jsoniter "github.com/json-iterator/go"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

const (
	Golang  = "go"
	Java    = "java"
	Python3 = "python3"
	CSharp  = "csharp"
	OC      = "oc"

	Lua = "lua"
)

const (
	TypeBool      = "bool"
	TypeBoolean   = "boolean"
	TypeInt       = "int"
	TypeInt32     = "int32"
	TypeLong      = "long"
	TypeInt64     = "int64"
	TypeFloat     = "float"
	TypeFloat32   = "float32"
	TypeDouble    = "double"
	TypeFloat64   = "float64"
	Typestring    = "string"
	TypeString    = "String"
	TypeLocString = "locstring"

	TypeBoolArray      = "bool[]"
	TypeBooleanArray   = "boolean[]"
	TypeIntArray       = "int[]"
	TypeInt32Array     = "int32[]"
	TypeLongArray      = "long[]"
	TypeInt64Array     = "int64[]"
	TypeFloatArray     = "float[]"
	TypeFloat32Array   = "float32[]"
	TypeDoubleArray    = "double[]"
	TypeFloat64Array   = "float64[]"
	TypestringArray    = "string[]"
	TypeStringArray    = "String[]"
	TypeLocStringArray = "locstring[]"
)

const (
	TestTplFileName = "config.tplt.txt"
)

const (
	FlagLineCS        = iota // 第1行：区分 客户端/服务端的标识
	FlagLineFieldType        // 第2行：定义的数据类型
	FlagLineFieldName        // 第3行：定义的字段名称
	FlagLineFieldDesc        // 第4行：定义的字段描述说明
)

const (
	GenClient = "c"
	GenServer = "s"
)

var Config *ConfigInfo

var DevLanguage = ""

func Export() {
	var devLang = flag.String("dev", "go", "开发语言-默认go")
	var lang = flag.String("lang", "zhCN", "多语言版本-默认中文")
	var csSign = flag.String("cs", "s", "导出端-默认服务端")
	var exportFile = flag.String("file", "all", "指定文件名-默认全部")
	flag.Parse()

	DevLanguage = strings.ToLower(*devLang)

	Config = loadConfig()

	var excelTo IExcelTo
	switch DevLanguage {
	case Golang:
		Config.setPackage(Config.GoPackage)
		excelTo = NewExcelToGo(*lang, *exportFile, *csSign)
	case Java:
		Config.setPackage(Config.JavaPackage)
		excelTo = NewExcelToJava(*lang, *exportFile, *csSign)
	case Lua:
		excelTo = NewExcelToLua(*lang, *exportFile, *csSign)
	default:
		LogError("× 请填写正确的开发语言")
		Log("- %v", Golang)
		Log("- %v", Java)
		Log("- %v", Python3)
		Log("- %v", CSharp)
		Log("- %v", OC)
		return
	}

	excelTo.cleanGenFile() // 清空生成目录
	excelTo.processExcel() // 处理excel
}

// 获取指定文件夹下所有文件的路径List
func GetFilePathList(path string) (filePathList []string, err error) {
	rd, err := ioutil.ReadDir(path)
	if err != nil {
		LogError("× read dir fail:%v \npath:%v", err, path)
		return
	}

	for _, fi := range rd {
		if !fi.IsDir() {
			fullName := path + "/" + fi.Name()
			filePathList = append(filePathList, fullName)
		}
	}
	return
}

// map转换成json
func GenJsonFile(resultList []map[string]interface{}, entityName, genDir string) (success bool) {
	var genJsonDir = filepath.Join(genDir, "json")
	// 判断文件夹是否存在
	utils.DirExists(genJsonDir)

	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	jsonBytes, err := json.MarshalIndent(resultList, "", "  ") // json.Marshal(resultList) //
	if err != nil {
		LogError("× 转换成json失败: %v", err)
		return
	}

	var fileName = fmt.Sprintf("%v.json", entityName)
	WriteJsonFile(genJsonDir, fileName, jsonBytes)
	success = true
	return
}

// 获取生成的实例名称
// excelFileName: 纯英文的excel名称：param.xlsx，而不是原名 param_参数表.xlsx
func GetEntityName(excelFileName string) string {
	// excel名称为param，拼接成 ConfParam
	var fileName = excelFileName[0:strings.Index(excelFileName, ".")]
	var entityName = fmt.Sprintf("%v%v", strings.ToUpper(fileName[:1]), fileName[1:])
	if DevLanguage != Golang {
		entityName = fmt.Sprintf("Conf%v", entityName)
	}
	return entityName
}

/**
 * 验证定义的字段是否有大小写不同的重复值
 * @param m
 */
func CheckFieldNameRepeat(list []*EntityField) {
	var judgeKeyList []string
	for _, v := range list {
		var key = strings.ToLower(v.FieldName)
		if utils.StrListContains(judgeKeyList, key) {
			LogError("× 配置中存在重复字段:%v", v.FieldName)
		}
		judgeKeyList = append(judgeKeyList, key)
	}

}

// json写入文本
func WriteJsonFile(dir, fileName string, jsonBytes []byte) {
	var filePath = filepath.Join(dir, fileName)
	utils.PathExists(filePath)
	err := ioutil.WriteFile(filePath, jsonBytes, 0666) //写入文件(字节数组)
	if err != nil {
		panic(fmt.Sprintf("× 写%v失败 \nerr:%v", filePath, err))
	}
}

// entity实例写入文本
func WriteEntityFile(dir, fileName string, code []byte) {
	var filePath = filepath.Join(dir, fileName)
	file, err := os.Create(filePath)
	if err != nil {
		LogError("× 创建实体文件失败 路径：'%s' err: %s", filePath, err)
	}
	if _, err := file.Write(code); err != nil {
		LogError("× 写实体文件失败，路径：'%s' err: %s", filePath, err)
	}
	file.Close()
}

func Log(str string, param ...interface{}) {
	fmt.Println(fmt.Sprintf(str, param...))
}

func LogError(str string, param ...interface{}) {
	fmt.Println("×.×.×.×.×.×.×.×")
	fmt.Println(fmt.Sprintf(str, param...))
}