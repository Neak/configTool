// Author：NeakHuang
// CreateDate: 
// Modifier：
// ModDate：
// Desc: 
package app

import (
	"fmt"
	"strings"
	"testing"
)

func TestParse(t *testing.T) {
	var s = "> < > <"
	var b = []byte(s)
	fmt.Println(string(b))


	//var src = []int{1,2,3,4}
	var src = []string{"1","2","3","4"}
	//var temp = make([]string, len(src))
	//for k, v := range src {
	//	temp[k] = fmt.Sprintf("%d", v)
	//}
	var result = fmt.Sprintf("{\"%v\"}", strings.Join(src, `","`))
	fmt.Println(result)
}