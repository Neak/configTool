// Author：NeakHuang
// CreateDate: 2020/4/9
// Modifier：
// ModDate：
// Desc: excel转成各种语言的接口，抽象类
package app

import (
	"configTool/utils"
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
	"path"
	"path/filepath"
	"strings"
)

// 接口类
type IExcelTo interface {
	cleanContainer()
	cleanGenFile()
	processExcel()
	processWorkbook(filePath, excelFileName string) (success bool)
	processSheet(rows [][]string) (success bool)
	startGen(entityName, lang string) (success bool)
	genApiConfig()
	buildEntityTemplate(entityName, lang string, genFlag string) interface{}

	// 获取sn类型
	getSnType(factFieldType string) (sn string)
	// 获取对应开发语言中的实际类型
	toDevLangType(fieldType string) (factType string)
	// 获取实际值类型
	processValByType(cellVal string, fieldType string) (factVal interface{})
	// 插入值：<FieldName, val>
	putFieldValueInMap(valMap map[string]interface{}, factFieldVal interface{}, fieldName string)
}

// 抽象基类
type AbstractExcelTo struct {
	real IExcelTo

	language   string // 需要导出的语言版本
	exportFile string // 指定导出文件 ALL则是全部

	genDir string // 生成路径，开发语言继承后赋值为 genDir + devLang 例如golang: genGo

	errorFileNames  []string // 错误的文件名记录
	snType          string   // sn类型
	excelOriginName string   // 配置表原名 例如：param_参数表.xlsx
	excelFileName   string   // 去掉注释的纯英文名 例如：param.xlsx (param_参数表.xlsx)

	// 导出数据
	serverResultList []map[string]interface{}
	clientResultList []map[string]interface{}

	// 生成对象记录  <字段名称，字段实体信息>
	serverEntityFieldList []*EntityField // 服务端字段实体
	clientEntityFieldList []*EntityField // 客户端字段实体

	snList []interface{} // 记录所有sn，判断是否有重复

	csSign string // 执行时，指定导出服务端/客户端标识

	cFieldNames []string // 验证c字段是否有重复定义
	sFieldNames []string // 验证s字段是否有重复定义

	entityApi *EntityApi // 记录所有文件实体信息

	processConfigCount int32 // 处理的配置文件总数
	ignoreJsonCount    int32 // 忽略的Json文件总数
	ignoreEntityCount  int32 // 忽略的实体文件总数
	failEntityCount    int32 // 处理失败文件总数
}

func NewAExcelTo(lang, exportFile, csSign string) *AbstractExcelTo {
	return &AbstractExcelTo{
		language:              lang,
		exportFile:            exportFile,
		errorFileNames:        make([]string, 0),
		snType:                "string",
		serverResultList:      make([]map[string]interface{}, 0),
		clientResultList:      make([]map[string]interface{}, 0),
		serverEntityFieldList: make([]*EntityField, 0),
		clientEntityFieldList: make([]*EntityField, 0),
		csSign:                strings.ToLower(csSign),
		cFieldNames:           make([]string, 0),
		sFieldNames:           make([]string, 0),
		entityApi:             &EntityApi{Package: Config.GoApiPackage, Import: Config.GoApiImport, ConfigPackage: Config.GoPackage},
		processConfigCount:    0,
		ignoreJsonCount:       0,
		ignoreEntityCount:     0,
		failEntityCount:       0,
	}
}

// 初始化实际子类
func (e *AbstractExcelTo) initReal(excelTo IExcelTo, devLang string) {
	e.real = excelTo
	e.genDir = fmt.Sprintf("%v%v_%v", Config.GenPath, strings.ToUpper(devLang), e.language)
	if devLang == Golang {
		// golang的目录结构修改为
		// GoApiPackage/api.go
		// GoApiPackage/GoPackage/*.go
		// GoApiPackage/json/*.json
		e.genDir = fmt.Sprintf("%v/%v", e.genDir, Config.GoApiPackage)
	}
}

// 清空生成的文件
func (e *AbstractExcelTo) cleanGenFile() {
	Log("√ 清理已生成的<服务端>数据目录:%v", e.genDir)
	utils.RemoveContents(e.genDir)
}

// 处理所有excel
// files：所有的文件名
func (e *AbstractExcelTo) processExcel() {
	if e.real == nil {
		panic("× 无法使用抽象类，请实例化对应的开发语言版本\n- 例如：NewExcelToGo")
	}

	// 加载所有配置文件
	var filePathList, err = GetFilePathList(Config.ConfigPath)
	if err != nil {
		LogError("× 配置表文件夹路径读取失败：%v", Config.ConfigPath)
		return
	}

	Log(" --------------------- ")
	Log("√ 发现文件 %v 个", len(filePathList))
	var success = false
	for _, filePath := range filePathList {
		fileName := filepath.Base(filePath) // 文件名 abc.xlsx
		fileSuffix := path.Ext(filePath)    // 文件名后缀 .xlsx
		if !strings.Contains(fileSuffix, ".xls") &&
			!strings.ContainsAny(fileSuffix, ".xlsx") &&
			!strings.ContainsAny(fileSuffix, ".xlsm") {
			LogError("× 忽略非法文件 %v", fileName)
			continue
		}
		if strings.Contains(fileName, "~$") {
			LogError("× 忽略临时文件 %v", fileName)
			continue
		}

		// 配置表原名：param_参数表.xlsx
		e.excelOriginName = fileName
		e.excelFileName = fileName
		// 处理成配置表去掉说明的名称：param.xlsx
		if strings.Contains(fileName, "_") {
			var fileFactName = strings.Split(fileName, "_")[0]
			e.excelFileName = fmt.Sprintf("%v%v", fileFactName, fileSuffix)
		}
		Log(" --------------------- ")
		Log("√ 开始处理：< %v >", filePath)
		e.processConfigCount++

		// 开始处理excel
		success = e.processWorkbook(filePath, e.excelFileName)
		if !success {
			break
		}
	}

	// 生成工具类
	funcGenApiConfig(e.real)

	//if success {
	Log("\n --------------------- ")
	Log(" ooo○ 处理完毕 ○ooo")
	Log(" √ 处理配置文件 %v 个", e.processConfigCount)
	Log(" × 忽略JSON文件 %v 个", e.ignoreJsonCount)
	Log(" × 忽略实体文件 %v 个", e.ignoreEntityCount)
	//Log(" ×生成实体失败 %v 个", e.failEntityCount)
	Log(" --------------------- ")
	//} else {
	//	Log("\n --------------------- ")
	//	LogError("× 终止！生成 %v 表格数据错误...", e.excelOriginName)
	//}

}

// 处理单个数据薄 excel
// filePath: excel路径 (e://project/config/param_参数.xlsx)
// excelFileName: excel去掉备注的名称（param.xlsx）
// return bool 是否处理成功
func (e *AbstractExcelTo) processWorkbook(filePath, excelFileName string) (success bool) {
	workbook, err := excelize.OpenFile(filePath)
	if err != nil {
		println(err.Error())
		return
	}

	var lang = e.language
	// 判断
	if strings.ToUpper(e.exportFile) != "ALL" &&
		!strings.Contains(excelFileName, e.exportFile) {
		LogError("× 忽略非指定文件 %v 个 \n", filePath)
		return
	}

	// 获取指定语言工作表上所有单元格
	var rows = workbook.GetRows(lang)
	if len(rows) <= 0 {
		LogError("× 语言：%v 数据表没有数据 %v. \n", lang, excelFileName)
		return
	}

	// 处理对应语言sheet
	success = funcProcessSheet(e.real, rows)

	// 获取实例名称
	var entityName = GetEntityName(excelFileName)
	// 开始处理生成
	if success = funcStartGen(e.real, entityName, lang); !success {
		e.failEntityCount++
	}

	// 生成完成，清理容器缓存
	funcCleanContainer(e.real)
	return
}

// 处理单个数据表 sheet
// rows：所有行数据
func (e *AbstractExcelTo) processSheet(rows [][]string) (success bool) {
	e.sFieldNames = make([]string, 0)
	e.cFieldNames = make([]string, 0)

	for rowIdx, row := range rows {
		// 服务端数据
		var serverMap = make(map[string]interface{})
		// 客户端数据
		var clientMap = make(map[string]interface{})

		for colIdx, cellVal := range row {
			var csFlag = e.getFieldCS(rows, colIdx) // 转成小写
			// 第1行的时候，头信息存储好（处理前四行数据）
			if rowIdx == FlagLineCS {
				// 忽略没有定义cs的列
				if len(csFlag) <= 0 {
					continue
				}
				e._saveHeadInfo(rows, colIdx, csFlag)
			}
			// 前4行，定义头信息
			if rowIdx <= FlagLineFieldDesc {
				continue
			}

			// 字段类型
			var fieldType, factFieldType, fieldName, _ = e.getFieldInfo(rows, colIdx)
			if factFieldType == "" || fieldName == "" {
				continue
			}

			// 检查第一列sn是否存在，不存在则忽略此行，并且抛出异常日志
			if colIdx == 0 {
				// sn统一为小写
				fieldName = strings.ToLower(fieldName)
				// 检查sn是否存在
				e.checkSnAndSaveType(cellVal, factFieldType, rowIdx)
			}

			// 获取数据实际类型的值 interface{}
			var factFieldVal = funcProcessValByType(e.real, cellVal, fieldType)

			// 服务端数据
			if strings.Contains(csFlag, "s") && strings.Contains(e.csSign, "s") {
				// 将值插入serverMap，<FieldName, fieldVal>
				//funcPutFieldValueInMap(e.real, serverMap, factFieldVal, FieldName)
				serverMap[fieldName] = factFieldVal
			}

			// 客户端数据
			if strings.Contains(csFlag, "c") && strings.Contains(e.csSign, "c") {
				// 将值插入serverMap，<FieldName, fieldVal>
				//funcPutFieldValueInMap(e.real, clientMap, factFieldVal, FieldName)
				clientMap[fieldName] = factFieldVal
			}
		}

		if len(serverMap) > 0 {
			e.serverResultList = append(e.serverResultList, serverMap)
		}

		if len(clientMap) > 0 {
			e.clientResultList = append(e.clientResultList, clientMap)
		}
	}

	return true
}

// 生成json文件
func (e *AbstractExcelTo) genJsonFile(resultList []map[string]interface{}, entityName string) (success bool) {
	// 生成json
	if success = GenJsonFile(resultList, entityName, e.genDir); !success {
		e.ignoreJsonCount++
	}
	return
}

// 接口方法：清空所有容器
func funcCleanContainer(excelTo IExcelTo) {
	excelTo.cleanContainer()
}

// 接口方法：调用子类重写 toDevLangType
func funcProcessSheet(excelTo IExcelTo, rows [][]string) bool {
	return excelTo.processSheet(rows)
}

// 接口方法：调用子类重写 startGen
func funcStartGen(excelTo IExcelTo, entityName, lang string) bool {
	return excelTo.startGen(entityName, lang)
}

// 接口方法：调用子类重写 funcGenApiConfig
func funcGenApiConfig(excelTo IExcelTo) {
	excelTo.genApiConfig()
}

// 接口方法：调用子类重写 getSnType
func funcGetSnType(excelTo IExcelTo, factFieldType string) (snType string) {
	snType = excelTo.getSnType(factFieldType)
	return
}

// 接口方法：调用子类重写 toDevLangType
func funcToDevLangType(excelTo IExcelTo, fieldType string) (factType string) {
	factType = excelTo.toDevLangType(fieldType)
	return
}

// 接口方法：调用子类重写 processValByType
func funcProcessValByType(excelTo IExcelTo, cellVal string, fieldType string) (factVal interface{}) {
	factVal = excelTo.processValByType(cellVal, fieldType)
	return
}

// 接口方法：调用子类重写 putFieldValueInMap
func funcPutFieldValueInMap(excelTo IExcelTo, valMap map[string]interface{}, factFieldVal interface{}, fieldName string) {
	excelTo.putFieldValueInMap(valMap, factFieldVal, fieldName)
	return
}

// 开始处理生成
// 纯虚函数，子类重写
func (e *AbstractExcelTo) startGen(entityName, lang string) (success bool) {
	return
}

// 生成工具类
// 纯虚函数：子类重写
func (e *AbstractExcelTo) genApiConfig() {
	Log("----------------------\n")
	Log("√ 无需生成工具类")
}

// 获取对应开发语言中的实际类型
// 子类重写
func (e *AbstractExcelTo) getSnType(factFieldType string) (snType string) {
	return
}

// 获取对应开发语言中的实际类型
// 纯虚函数，子类重写
func (e *AbstractExcelTo) toDevLangType(fieldType string) (factType string) {
	return
}

// 获取实际值类型
// 纯虚函数，子类重写
func (e *AbstractExcelTo) processValByType(cellVal string, fieldType string) (factVal interface{}) {
	return
}

// 插入值：<FieldName, val>
// 纯虚函数，子类重写
func (e *AbstractExcelTo) putFieldValueInMap(valMap map[string]interface{}, factFieldVal interface{}, fieldName string) {
}

// 存储数据表实体对象头信息
// rows：所有行数据
// colIdx: 当前列序号
// csFlag：客户端服务端标记
func (e *AbstractExcelTo) _saveHeadInfo(rows [][]string, colIdx int, csFlag string) {
	// 字段类型
	var fieldType, factFieldType, fieldName, fieldDesc = e.getFieldInfo(rows, colIdx)
	if factFieldType == "" || fieldName == "" {
		LogError("× 类型：%v，描述：%v。该列没有声明字段\n", fieldType, fieldDesc)
		return
	}

	// 实体对象信息 <string, string>：｛字段类型：int，名称:参数1，注释：整形参数 ｝
	var entityField = &EntityField{
		FieldType:    factFieldType,
		FieldOriName: fieldName,                                                          // json显示时的key，首字母未做强制大写转换
		FieldName:    fmt.Sprintf("%v%v", strings.ToUpper(fieldName[:1]), fieldName[1:]), // 首字母大写
		FieldDesc:    strings.Replace(fieldDesc, "\n", "", -1),
	}

	var lowerFieldName = strings.ToLower(fieldName)
	// 判断服务端标识
	if strings.Contains(csFlag, "s") {
		e.serverEntityFieldList = append(e.serverEntityFieldList, entityField)

		// key重复
		if utils.StrListContains(e.sFieldNames, lowerFieldName) {
			LogError("× 有重复定义的字段:%v \n", fieldName)
			return
		}
		e.sFieldNames = append(e.sFieldNames, lowerFieldName)
	}

	// 判断客户端标识
	if strings.Contains(csFlag, "c") {
		e.clientEntityFieldList = append(e.clientEntityFieldList, entityField)

		// key重复
		if utils.StrListContains(e.cFieldNames, lowerFieldName) {
			LogError("× 有重复定义的字段:%v \n", fieldName)
			return
		}
		e.cFieldNames = append(e.cFieldNames, lowerFieldName)
	}

}

// 清除容器
func (e *AbstractExcelTo) cleanContainer() {
	// 清除已写入的容器
	e.serverResultList = make([]map[string]interface{}, 0)
	e.clientResultList = make([]map[string]interface{}, 0)
	e.serverEntityFieldList = make([]*EntityField, 0)
	e.clientEntityFieldList = make([]*EntityField, 0)
	e.snList = make([]interface{}, 0)
}

// 检查sn是否正确
func (e *AbstractExcelTo) checkSnAndSaveType(cellVal string, factFieldType string, rowIdx int) {
	if len(cellVal) <= 0 {
		panic(fmt.Sprintf("× sn不能为空，rowIndex:%v", rowIdx))
	}

	var sn = cellVal
	if utils.ListContains(e.snList, sn) {
		panic(fmt.Sprintf("× sn中有重复的值，sn:%v", sn))
	}
	e.snList = append(e.snList, sn)
	e.snType = funcGetSnType(e.real, factFieldType)
}

// 获取字段 cs标识，转成小写
// rows：所有行数据
// colIdx: 当前列序号
func (e *AbstractExcelTo) getFieldCS(rows [][]string, colIdx int) (csFlag string) {
	// cs标识
	csFlag = strings.ToLower(rows[FlagLineCS][colIdx])
	return
}

// 获取字段类型
// rows：所有行数据
// colIdx: 当前列序号
// return：FieldType:表中原数据类型；factFieldType实际语言数据类型；FieldName:字段名称，FieldDesc:字段说明
func (e *AbstractExcelTo) getFieldInfo(rows [][]string, colIdx int) (fieldType, factFieldType, fieldName, fieldDesc string) {
	// 字段类型
	fieldType = strings.ToLower(rows[FlagLineFieldType][colIdx])
	if fieldType != "" || len(fieldType) > 0 {
		// 转换为对应开发语言的实际类型
		factFieldType = funcToDevLangType(e.real, fieldType)
	}

	// 字段名称
	fieldName = rows[FlagLineFieldName][colIdx]

	// 字段描述说明
	fieldDesc = rows[FlagLineFieldDesc][colIdx]

	return
}

// 构建生成模板的实例对象
func (e *AbstractExcelTo) buildEntityTemplate(entityName, lang string, genFlag string) interface{} {
	// 是否忽略生成模板
	var isIgnore bool
	var entityFieldList []*EntityField
	if genFlag == GenServer {
		entityFieldList = e.serverEntityFieldList
	} else if genFlag == GenClient {
		entityFieldList = e.clientEntityFieldList
	} else {
		LogError("× 忽略实例，生成端标识错误 %v , genFlag(c|s)：%v", entityName, genFlag)
		isIgnore = true
	}
	if len(lang) <= 0 {
		LogError("× 忽略实例！多语言错误 %v ", entityName)
		isIgnore = true
	}
	if len(entityFieldList) <= 0 {
		LogError("× 忽略实例：没有需要生成的字段 %v ", entityName)
		isIgnore = true
	}

	if isIgnore {
		e.ignoreEntityCount++
		return nil
	}

	// 文件实体
	var entity = &EntityNameInfo{
		EntityName:      entityName,
		ExcelOriginName: e.excelOriginName,
	}

	// 所有文件实体信息
	e.entityApi.EntityNameInfoList = append(e.entityApi.EntityNameInfoList, entity)

	// 检查字段是否有大小写不一但是实际相同的重复值 例如，paramDemo, ParamDemo
	CheckFieldNameRepeat(entityFieldList)

	// 实体模板对象
	var entityTpl = &EntityTemplate{
		Package:         Config.devLangPkg,
		EntityName:      entityName,
		EntityLang:      lang,
		ExcelFileName:   e.excelFileName,
		ExcelOriginName: e.excelOriginName,
		SNType:          e.snType,
	}

	// 所有字段信息列表
	var fieldList []*EntityField
	for _, entityInfo := range entityFieldList {
		fieldList = append(fieldList, entityInfo)
	}
	entityTpl.EntityFieldList = fieldList
	return entityTpl
}
