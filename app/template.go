/**
 *	Author: Neak
 *	Date: 2020/4/22
 *  Desc: 模板操作类
 */
package app

import "strings"

// 实例Api模板信息
type EntityApi struct {
	Package            string
	Import            string
	ConfigPackage	   string
	EntityNameInfoList []*EntityNameInfo
}

// 实体信息（用于生成实体文件）
type EntityNameInfo struct {
	EntityName      string // 实体名称：ConfTestTable
	ExcelOriginName string // 配置表原名：TestTable_测试表.xlsx
}

// 实体字段信息（用预生成实体对象的字段名称）
type EntityField struct {
	FieldType    string // 字段类型
	FieldOriName string // 字段名称（原字段名，用于json中显示）
	FieldName    string // 字段名称（首字母大写）
	FieldDesc    string // 字段注释
}

// 实体模板对象（用于生成实体文件）
type EntityTemplate struct {
	Package         string // 包名
	EntityName      string // 实体名
	EntityLang      string // 实体多语言
	ExcelFileName   string // excel 文件纯英文名 param.xlsx
	ExcelOriginName string // excel 文件原名    param_参数表.xlsx
	SNType          string // sn数据类型

	EntityFieldList []*EntityField // 实体信息
}

type JavaEntityTemplate struct {
	*EntityTemplate
	CtorParamStr string // 构造参数字符串 String sn, int numInt, long numLong...
	InitParamStr string // 初始化是参数字符创 conf.getIntValue(%v), conf.getBooleanValue(%v)
}

type LuaEntityTemplate struct {
	*EntityTemplate
	RTList []string
	DataList []string
	DefaultList []string

	NormalList []string
}

func (e *EntityTemplate) EntityNameToLower() string {
	return strings.ToLower(e.EntityName)
}

// 工具模板对象
type ConfTemplate struct {
	AllEntityNames []string // 所有实体名称
}
