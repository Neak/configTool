package example;


import game.worldsvr.config.ConfTable;

public class main {
    public static void main(String[] args) throws InterruptedException {
        ConfTable.reLoad();
//        Thread.sleep(1000);
        ConfTable confTable = ConfTable.get(1);
        
        System.out.println("Sn = " + confTable.sn);                         // 序号
        System.out.println("StringVal = " + confTable.stringVal);           // 字符串值
        System.out.println("IntVal = " + confTable.intVal);                 // 整型值
        System.out.println("BoolVal = " + confTable.boolVal);               // 布尔值
        System.out.println("IntArray = " + confTable.intArray);             // 整型数组
        System.out.println("LongArray = " + confTable.longArray);           // 长整型数组
        System.out.println("FloatArray = " + confTable.floatArray);         // 单精度数组
        System.out.println("DoubleArray = " + confTable.doubleArray);       // 双精度数组
        System.out.println("BoolArray = " + confTable.boolArray);           // 布尔值数组
        System.out.println("ServerStrArray = " + confTable.serverStrArray); // 字符串数组
        System.out.println("----------------------------");
    }
}
