package core.support;

/**
 * @author NeakHuang
 * @date 2016.5.13
 * @desc 字段排序
 */

public class OrderByField {

	private String key = "";
	
	private OrderBy orderBy;
	
	public OrderByField(String key, OrderBy orderBy) {
		this.key = key;
		this.orderBy = orderBy;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public OrderBy getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(OrderBy orderBy) {
		this.orderBy = orderBy;
	}

}
