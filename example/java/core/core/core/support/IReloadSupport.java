package core.support;

/**
 * @author NeakHuang
 * @date 2016.5.13
 * @desc 数据加载接口
 */
public interface IReloadSupport {

	public void beforeReload();

	public void afterReload();

}
