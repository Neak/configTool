package core.support;

/**
 * @author NeakHuang
 * @date 2016.5.13
 * @desc 排序枚举
 */

public enum OrderBy {
	ASC, DESC
}
