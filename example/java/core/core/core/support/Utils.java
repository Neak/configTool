package core.support;

/**
 * @author NeakHuang
 * @date 2016.3.17
 * @desc 工具类
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.message.ParameterizedMessage;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Utils {
    
    public static boolean booleanValue(Boolean value) {
        if (null == value)
            return false;
        else
            return (boolean) value;
    }
    
    public static boolean booleanValue(Object value) {
        if (null == value)
            return false;
        else if (value instanceof Boolean) {
            return (boolean) value;
        } else {
            return booleanValue(value.toString());
        }
    }
    
    /**
     * String转为boolean型 如果出错 则为false
     *
     * @param value
     * @return
     */
    public static boolean booleanValue(String value) {
        if (value != null && "true".equalsIgnoreCase(value))
            return true;
        else
            return false;
    }
    
    public static int intValue(Integer value) {
        if (null == value)
            return 0;
        else
            return (int) value;
    }
    
    public static int intValue(Object value) {
        Double temp = doubleValue(value.toString());
        if (temp > Integer.MAX_VALUE) {
            return Integer.MAX_VALUE;
        } else {
            return temp.intValue();
        }
    }
    
    /**
     * String转为int型 如果出错 则为0
     *
     * @param value 类似"12.12"这种字符串
     * @return 返回12
     */
    public static int intValue(String value) {
        // return NumberUtils.toInt(value);// 不能解析"12.12"这种字符串
        int ret = 0;
        if (value != null) {
            value = value.trim();// 去掉前尾空格
            // 去掉小数点后面的字符
            int index = value.indexOf(".");
            if (index > -1) {
                value = value.substring(0, index);
            }
            ret = NumberUtils.toInt(value);
        }
        return ret;
    }
    
    public static long longValue(Long value) {
        if (null == value)
            return 0L;
        else
            return (long) value;
    }
    
    public static long longValue(Object value) {
        Double temp = doubleValue(value.toString());
        if (temp > Long.MAX_VALUE) {
            return Long.MAX_VALUE;
        } else {
            return temp.longValue();
        }
    }
    
    /**
     * String转为long型 如果出错 则为0L
     *
     * @param value 类似"12.12"这种字符串
     * @return 返回12
     */
    public static long longValue(String value) {
        // return NumberUtils.toLong(value);// 不能解析"12.12"这种字符串
        long ret = 0L;
        if (value != null) {
            value = value.trim();// 去掉前尾空格
            // 去掉小数点后面的字符
            int index = value.indexOf(".");
            if (index > -1) {
                value = value.substring(0, index);
            }
            ret = NumberUtils.toLong(value);
        }
        return ret;
    }
    
    public static double doubleValue(Double value) {
        if (null == value)
            return 0.0D;
        else
            return round2((double) value);// float保留1位小数，double保留2位小数
    }
    
    public static double doubleValue(Object value) {
        return doubleValue(value.toString());
    }
    
    /**
     * String转为double型 如果出错 则为0.0D
     *
     * @param value 类似"12.12"这种字符串
     * @return 返回12.12
     */
    public static double doubleValue(String value) {
        if (StringUtils.isNotEmpty(value)) {// value != null
            value = value.trim();// 去掉前尾空格
            double ret = NumberUtils.toDouble(value);
            return round2(ret);// float保留1位小数，double保留2位小数
        } else {
            return 0.0D;
        }
    }
    
    public static float floatValue(Float value) {
        if (null == value)
            return 0.0F;
        else
            return round((float) value);// float保留1位小数，double保留2位小数
    }
    
    public static float floatValue(Object value) {
        Double temp = doubleValue(value.toString());
        if (temp > Float.MAX_VALUE) {
            return Float.MAX_VALUE;
        } else {
            return round(temp.floatValue());// float保留1位小数，double保留2位小数
        }
    }
    
    /**
     * String转为float型 如果出错 则为0.0f
     *
     * @param value 类似"12.12"这种字符串
     * @return 返回12.12
     */
    public static float floatValue(String value) {
        float ret = 0.0F;
        if (value != null) {
            value = value.trim();// 去掉前尾空格
            ret = NumberUtils.toFloat(value);
        }
        return round(ret);// float保留1位小数，double保留2位小数
    }
    
    /**
     * 保留一位小数点
     *
     * @param value 如100/3.0f
     * @return 返回33.3f
     */
    public static float round(float value) {
        return Math.round(value * 10.0f) / 10.0f;
    }
    
    /**
     * 保留一位小数点
     *
     * @param value 如100/3.0d
     * @return 返回33.3d
     */
    public static double round(double value) {
        return Math.round(value * 10.0d) / 10.0d;
    }
    
    /**
     * 保留两位小数点
     *
     * @param value 如100/3.0f
     * @return 返回33.33f
     */
    public static float round2(float value) {
        return Math.round(value * 100.0f) / 100.0f;
    }
    
    /**
     * 保留两位小数点
     *
     * @param value 如100/3.0d
     * @return 返回33.33d
     */
    public static double round2(double value) {
        return Math.round(value * 100.0d) / 100.0d;
    }
    
    
    /**
     * 基于参数创建字符串 #0开始
     *
     * @param str
     * @param params
     * @return
     */
    public static String createStr(String str, Object... params) {
        return ParameterizedMessage.format(str, params);
    }
    
    /**
     * 字符串转换成boolean[]：分隔符=","
     */
    public static boolean[] strToBoolArray(String str) {
        return strToBoolArray(str, ",");
    }
    
    /**
     * 字符串转换成boolean[]
     *
     * @param str   字符串
     * @param split 分隔符（特殊符号必须加 转义符 \\）：如"\\|"
     * @return
     */
    public static boolean[] strToBoolArray(String str, String split) {
        boolean[] ret = null;
        if (StringUtils.isNotBlank(str)) {
            String[] strAry = str.split(split);
            if (strAry != null) {
                int size = strAry.length;
                ret = new boolean[size];
                for (int i = 0; i < size; i++) {
                    ret[i] = Utils.booleanValue(strAry[i]);
                }
            }
        }
        return ret;
    }
    
    /**
     * 字符串转换成int[]：分隔符=","
     */
    public static int[] strToIntArray(String str) {
        return strToIntArray(str, ",");
    }
    
    /**
     * 字符串转换成int[]
     *
     * @param str   字符串
     * @param split 分隔符（特殊符号必须加 转义符 \\）：如"\\|"
     * @return
     */
    public static int[] strToIntArray(String str, String split) {
        int[] ret = null;
        if (StringUtils.isNotBlank(str)) {
            String[] strAry = str.split(split);
            if (strAry != null) {
                int size = strAry.length;
                ret = new int[size];
                for (int i = 0; i < size; i++) {
                    ret[i] = Utils.intValue(strAry[i]);
                }
            }
        }
        return ret;
    }
    
    /**
     * 字符串转换成long[]：分隔符=","
     */
    public static long[] strToLongArray(String str) {
        return strToLongArray(str, ",");
    }
    
    /**
     * 字符串转换成long[]
     *
     * @param str   字符串
     * @param split 分隔符（特殊符号必须加 转义符 \\）：如"\\|"
     * @return
     */
    public static long[] strToLongArray(String str, String split) {
        long[] ret = null;
        if (StringUtils.isNotBlank(str)) {
            String[] strAry = str.split(split);
            if (strAry != null) {
                int size = strAry.length;
                ret = new long[size];
                for (int i = 0; i < size; i++) {
                    ret[i] = Utils.longValue(strAry[i]);
                }
            }
        }
        return ret;
    }
    
    /**
     * 字符串转换成float[]：分隔符=","
     */
    public static float[] strToFloatArray(String str) {
        return strToFloatArray(str, ",");
    }
    
    /**
     * 字符串转换成float[]
     *
     * @param str   字符串
     * @param split 分隔符（特殊符号必须加 转义符 \\）：如"\\|"
     * @return
     */
    public static float[] strToFloatArray(String str, String split) {
        float[] ret = null;
        if (StringUtils.isNotBlank(str)) {
            String[] strAry = str.split(split);
            if (strAry != null) {
                int size = strAry.length;
                ret = new float[size];
                for (int i = 0; i < size; i++) {
                    ret[i] = Utils.floatValue(strAry[i]);
                }
            }
        }
        return ret;
    }
    
    
    /**
     * 字符串转换成double[]：分隔符","
     */
    public static double[] strToDoubleArray(String str) {
        return strToDoubleArray(str, ",");
    }
    
    /**
     * 字符串转换成double[]
     *
     * @param str   字符串
     * @param split 分隔符（特殊符号必须加 转义符 \\）：如"\\|"
     * @return
     */
    public static double[] strToDoubleArray(String str, String split) {
        double[] ret = null;
        if (StringUtils.isNotBlank(str)) {
            String[] strAry = str.split(split);
            if (strAry != null) {
                int size = strAry.length;
                ret = new double[size];
                for (int i = 0; i < size; i++) {
                    ret[i] = Utils.doubleValue(strAry[i]);
                }
            }
        }
        return ret;
    }
    
    /**
     * 字符串转换成String[]：分隔符=","
     */
    public static String[] strToStrArray(String str) {
        return strToStrArray(str, ",");
    }
    
    
    /**
     * 字符串转换成String[]
     *
     * @param str   字符串
     * @param split 分隔符（特殊符号必须加 转义符 \\）：如"\\|"
     * @return
     */
    public static String[] strToStrArray(String str, String split) {
        String[] ret = null;
        if (StringUtils.isNotBlank(str)) {
            ret = str.split(split);
        }
        return ret;
    }
    
    /**
     * 获取运行类所在根目录的路径
     *
     * @return
     */
    public static String getClassPath() {
        String ret = "";
        URL url = Thread.currentThread().getContextClassLoader().getResource("");
        if (url != null) {
            ret = url.getPath();
        }
        return ret;
    }
    
    /**
     * 获取运行类所在根目录下的指定文件或目录的路径
     *
     * @param name
     * @return
     */
    public static String getClassPath(String name) {
        String ret = "";
        URL url = Thread.currentThread().getContextClassLoader().getResource(name);
        if (url != null) {
            ret = url.getPath();
        }
        return ret;
    }
    
    /**
     * 构造List对象 如果传入的是参数仅仅为一个对象数组(Object[])或原生数组(int[], long[]等)
     * 那么表现结果表现是不同的，Object[]为[obj[0], obj[1], obj[2]] 而原生数组则为[[int[0],
     * int[1]，int[2]]] 多了一层嵌套，需要对原生数组进行特殊处理。
     *
     * @param <T>
     * @param ts
     * @return
     */
    @SafeVarargs
    @SuppressWarnings({"rawtypes", "unchecked"})
    public static <T> List<T> ofList(T... ts) {
        List result = new ArrayList();
        
        // 对Null进行特殊处理
        if (ts == null) {
            result.add(null);
            return result;
        }
        
        // 对单独的原始数组类型进行特殊处理
        if (ts.length == 1 && ts[0] != null && OFLIST_ARRAY_CLASS.contains(ts[0].getClass())) {
            if (ts[0] instanceof int[]) {
                int[] val = (int[]) ts[0];
                for (int v : val) {
                    result.add(v);
                }
            } else if (ts[0] instanceof long[]) {
                long[] val = (long[]) ts[0];
                for (long v : val) {
                    result.add(v);
                }
            } else if (ts[0] instanceof boolean[]) {
                boolean[] val = (boolean[]) ts[0];
                for (boolean v : val) {
                    result.add(v);
                }
            } else if (ts[0] instanceof byte[]) {
                byte[] val = (byte[]) ts[0];
                for (byte v : val) {
                    result.add(v);
                }
            } else if (ts[0] instanceof double[]) {
                double[] val = (double[]) ts[0];
                for (double v : val) {
                    result.add(v);
                }
            }
        } else { // 对象数组
            for (T t : ts) {
                result.add(t);
            }
        }
        
        return result;
    }
    
    // 专供ofList类使用 对于数组类型进行特殊处理
    private static final List<?> OFLIST_ARRAY_CLASS = Utils.ofList(int[].class, long[].class, boolean[].class, byte[].class, double[].class);
}
