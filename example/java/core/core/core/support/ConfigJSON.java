package core.support;

/**
 * @author NeakHuang
 * @date 2016.5.13
 * @desc 配置json基类
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface ConfigJSON {

}