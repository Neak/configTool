local ConfTable = require("ConfTable")
local confTable = ConfTable.Get(1)

print("Sn = ",confTable.sn)                         -- 序号
print("StringVal = ",confTable.stringVal)           -- 字符串值
print("IntVal = ",confTable.intVal)                 -- 整型值
print("BoolVal = ",confTable.boolVal)               -- 布尔值
print("IntArray = ",confTable.intArray)             -- 整型数组
print("LongArray = ",confTable.longArray)           -- 长整型数组
print("FloatArray = ",confTable.floatArray)         -- 单精度数组
print("DoubleArray = ",confTable.doubleArray)       -- 双精度数组
print("BoolArray = ",confTable.boolArray)           -- 布尔值数组
print("ServerStrArray = ",confTable.serverStrArray) -- 字符串数组
print("----------------------------")
