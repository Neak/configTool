// Author：NeakHuang
// CreateDate: 2020/5/30
// Modifier：
// ModDate：
// Desc: 
package src

import (
	conf "configTool/example/go/src/core/config"
	"fmt"
	"testing"
)

var jsonPath = "./json"
var Conf *conf.Conf

func init() {
	// 方式1，静态加载
	conf.InitAllTable(jsonPath)
	// 方式2，对象化加载
	Conf = conf.NewConf(jsonPath)
}

func TestConfig(t *testing.T) {
	StaticGet()
	ObjectGet()
}

func StaticGet() {
	fmt.Println("----------------------------")
	fmt.Println("静态方法形式获取")
	var confTable = conf.GetConfTable(1)
	fmt.Println(fmt.Sprintf("Sn = %v", confTable.Sn))                         // 序号
	fmt.Println(fmt.Sprintf("StringVal = %v", confTable.StringVal))           // 字符串值
	fmt.Println(fmt.Sprintf("IntVal = %v", confTable.IntVal))                 // 整型值
	fmt.Println(fmt.Sprintf("BoolVal = %v", confTable.BoolVal))               // 布尔值
	fmt.Println(fmt.Sprintf("IntArray = %v", confTable.IntArray))             // 整型数组
	fmt.Println(fmt.Sprintf("LongArray = %v", confTable.LongArray))           // 长整型数组
	fmt.Println(fmt.Sprintf("FloatArray = %v", confTable.FloatArray))         // 单精度数组
	fmt.Println(fmt.Sprintf("DoubleArray = %v", confTable.DoubleArray))       // 双精度数组
	fmt.Println(fmt.Sprintf("BoolArray = %v", confTable.BoolArray))           // 布尔值数组
	fmt.Println(fmt.Sprintf("ServerStrArray = %v", confTable.ServerStrArray)) // 字符串数组
	fmt.Println("----------------------------")
}

func ObjectGet() {
	fmt.Println("----------------------------")
	fmt.Println("- 对象形式获取")
	var confTable = conf.ConfTable.Get(1)
	fmt.Println(fmt.Sprintf("Sn = %v", confTable.Sn))                         // 序号
	fmt.Println(fmt.Sprintf("StringVal = %v", confTable.StringVal))           // 字符串值
	fmt.Println(fmt.Sprintf("IntVal = %v", confTable.IntVal))                 // 整型值
	fmt.Println(fmt.Sprintf("BoolVal = %v", confTable.BoolVal))               // 布尔值
	fmt.Println(fmt.Sprintf("IntArray = %v", confTable.IntArray))             // 整型数组
	fmt.Println(fmt.Sprintf("LongArray = %v", confTable.LongArray))           // 长整型数组
	fmt.Println(fmt.Sprintf("FloatArray = %v", confTable.FloatArray))         // 单精度数组
	fmt.Println(fmt.Sprintf("DoubleArray = %v", confTable.DoubleArray))       // 双精度数组
	fmt.Println(fmt.Sprintf("BoolArray = %v", confTable.BoolArray))           // 布尔值数组
	fmt.Println(fmt.Sprintf("ServerStrArray = %v", confTable.ServerStrArray)) // 字符串数组
	fmt.Println("----------------------------")
}
