echo on

:: 模块路径
@SET ModPath=..\

:: 模块名称
@SET ModName=configTool
:: 进程包路径
@SET SvrPackPath=%ModName%

:: 输出到的文件夹
@SET OutputPath=bin\

:: 编译linux二进制
@SET GOARCH=amd64
@SET GOOS=linux
@SET CGO_ENABLED=0

:: 转到模块路径
cd %ModPath%

:: 编译开始时间
@SET t=%time%

go build -o %OutputPath% %SvrPackPath%

:: 编译结束时间，计算编译时间
@SET t1=%time%
@IF "%t1:~,2%" lss "%t:~,2%" set "add=+24"
@SET /a "times=(%t1:~,2%-%t:~,2%%add%)*360000+(1%t1:~3,2%%%100-1%t:~3,2%%%100)*6000+(1%t1:~6,2%%%100-1%t:~6,2%%%100)*100+(1%t1:~-2%%%100-1%t:~-2%%%100)" ,"ss=(times/100)%%60","mm=(times/6000)%%60","hh=times/360000","ms=times%%100"

@echo build complete: %OutputPath%\%1 %BuildTime%; useTime:%ss%.%ms%(sec)

pause
