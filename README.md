# configTool

[TOC]

## 介绍

- 游戏开发配置工具，不再为配置表代码发愁
- 将Excel配置，转换成对应的json文件，以及生成对应语言的实体对象。开发过程中，减少编写配置表相关的代码。
- 基于go.template开发


<details>
<summary>目录结构</summary>
<pre><code>.
├── Owner: Neakhuang
├── bin		// 生成目录
├── config 	// excel配置表
├── example // 使用示例
├── gen...  // 生成目录
├── template// 模板文件
├── utils   // 工具类
├── vendor  // 依赖库
└── utils   // 工具类
</code></pre>
</details>

## 使用说明

**下载release最新发布版本**

- 已提供部分bat脚本

**win下命令行执行：**

- ```bash
  configTool.exe -dev go -lang zhCN -cs c -file all
  ```

**linux下：**

- ```bash
  configTool -dev go -lang zhCN -cs c -file all
  ```

| 参数 | 说明                          |
| ---- | ----------------------------- |
| dev  | 开发语言，默认值go            |
| lang | 语言版本，默认值zhCN          |
| cs   | 前后端标识，默认值c           |
| file | 指定文件，默认值all，代表所有 |

- 开发语言目前支持：**java，go，lua**

## 配置表规范

**第一行：cs标识，不填为不导出字段**

| 格式 | 说明         |
| ---- | ------------ |
| c    | 客户端用字段 |
| s    | 服务端用字段 |
| cs   | 双端用字段   |

**第二行：数据类型**

| 格式                           | 说明             |
| ------------------------------ | ---------------- |
| bool，boolean                  | 布尔值           |
| int，int32                     | 整型             |
| long，int64                    | 长整型           |
| float，float32                 | 单精度浮点数     |
| double，float64                | 双精度浮点数     |
| sting，String，locstring       | 字符串           |
| bool[]，boolean[]              | 布尔值数组       |
| int[]，int32[]                     | 整型数组         |
| long[]，int64[]                    | 长整型数组       |
| float[]，float32[]                 | 单精度浮点数数组 |
| double[]，float64[]                | 双精度浮点数数组 |
| sting[]，String[]，locstring[] | 字符串数组       |

- 数组使用时，分隔符,（逗号）
  - **[1,2,3,4,5]** -> **1,2,3,4,5**

- 二维数组使用时，数据类型使用字符串，需要自行处理，内层分隔符|（竖线），外层分隔符,（逗号）
  - **[[1,2,3],[11,22,33]]** -> **1|2|3,11|22|33**

**第三行：字段定义，字段名称**

**第四行：字段说明，注释**

详细参考 ./bin/config/Table_测试表.xlsx

## 示例example

- 具体参考example

## 优化

- **Lua表优化**
  - 多数使用lua时，都是直接将配置生成为lua表，如果配置表中存在大量重复数据，lua表占用内存，及文件必然增大。
  - 导出配置时，利用元表的特性对Lua的配置内容进行了优化
  - 优化内容：
    - 提取配置表中的所有数组，存放于一个LuaTable中
    - 将表中每个字段重复率最高的值，存在一个LuaTable中
    - 每一行数据中，与重复率最高不一致的值的数据，存在一个LuaTable中
- 优化参考：https://github.com/lujian101/LuaTableOptimizer

- **go生成文件**
  - 文件名不再带有conf前缀

## config.json配置说明

| 字段         | 说明                          |
| ------------ | ----------------------------- |
| configPath   | 配置表源文件相对路径          |
| templatePath | 模板文件相对路径              |
| genPath      | 生成结果文件相对路径          |
| GoApiName    | golang api文件的名称          |
| GoApiImport  | golang api文件引用的包路径    |
| GoApiPackage | golang api文件的包名          |
| GoPackage    | golang 配置文件生存实体的包名 |
| JavaPackage  | Java 配置文件生成实体的包名   |

## 发布版本

**v1.0**：初始版本

**v1.1**：修改为压缩包，解压即可使用

**v1.2**：修复模板错误问题，修复包命名错误问题

**v1.3**：golang版本生成包结构调整

## 参与贡献

1.  NeakHuang：https://gitee.com/Neak
