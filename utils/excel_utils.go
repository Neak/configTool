/**
 *	Author: Neak
 *	Date: 2020/4/18
 *  Desc:
 */
package utils

/**
 * 将excel数字转换为字母顺序
 * @param num
 * @return
 */
func ConvertNumToLetter(num int) string {
	var letter = []string{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X",
		"Y", "Z"}
	if num > 25 {
		tenPos := num/26 - 1
		onePos := num % 26
		return letter[tenPos] + letter[onePos]
	}
	return letter[num]
}
