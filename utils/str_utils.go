/**
 *	Author: Neak
 *	Date: 2020/4/19
 *  Desc:
 */
package utils

import (
	"bytes"
	"fmt"
	"go/format"
	"log"
	"strconv"
	"strings"
	"unsafe"
)

////////////////////////////////////////////////////////
/// 字符串转切片等操作
///////////////////////////////////////////////////////

// string字符串转intap eg： "10,20,30,40" -> {0=10, 1=20, 2=30, 3=40}
// str:转换字符串，默认分割为","
func StringToInt32Map(val string) (intMap map[int32]int32) {
	intMap = map[int32]int32{}
	if strings.Trim(val, " ") != "" {
		strAry := strings.Split(val, ",")
		for i, v := range strAry {
			intMap[int32(i)] = Int32Value(v)
		}
	}
	return intMap
}

// 字符串转[]string切片，分割符","
func StringToStrSlice(val string) (ret []string) {
	if strings.Trim(val, " ") != "" {
		ret = strings.Split(val, ",")
	}
	return ret
}

// 字符串转[]string切片，分割符"|"
func StringToStrSlice1(val string) (ret []string) {
	if strings.Trim(val, " ") != "" {
		ret = strings.Split(val, "|")
	}
	return ret
}

// 字符串转[]string切片，分割符自定义
func StringToStrSliceBySplit(str string, Split string) (ret []string) {
	if strings.Trim(str, " ") != "" {
		ret = strings.Split(str, Split)
	}
	return ret
}

// 字符串转[]bool切片，分割符","
func StringToBoolSlice(val string) (ret []bool) {
	strAry := StringToStrSlice(val)
	for _, s := range strAry {
		ret = append(ret, BoolValue(s))
	}
	return ret
}

// 字符串转[]bool切片，分割符"|"
func StringToBoolSlice1(str string) (ret []bool) {
	strAry := StringToStrSlice(str)
	for _, s := range strAry {
		ret = append(ret, BoolValue(s))
	}
	return ret
}

// 字符串转[]bool切片，分割符自定义
func StringToBoolSliceSplit(val string, split string) (ret []bool) {
	strAry := StringToStrSliceBySplit(val, split)
	for _, v := range strAry {
		ret = append(ret, BoolValue(v))
	}
	return ret
}

// 字符串转[]int切片，分割符","
func StringToIntSlice(val string) (ret []int) {
	strAry := StringToStrSlice(val)
	for _, v := range strAry {
		ret = append(ret, IntValue(v))
	}
	return ret
}

// 字符串转[]int切片，分割符"|"
func StringToIntSlice1(str string) (ret []int) {
	strAry := StringToStrSlice1(str)
	for _, v := range strAry {
		ret = append(ret, IntValue(v))
	}
	return ret
}

// 字符串转[]int切片，分割符自定义
func StringToIntSliceSplit(val string, split string) (ret []int) {
	strAry := StringToStrSliceBySplit(val, split)
	for _, v := range strAry {
		ret = append(ret, IntValue(v))
	}
	return ret
}

// 字符串转[]int32切片，分割符","
func StringToInt32Slice(val string) (ret []int32) {
	strAry := StringToStrSlice(val)
	for _, v := range strAry {
		ret = append(ret, Int32Value(v))
	}
	return ret
}

// 字符串转[]int32切片，分割符"|"
func StringToInt32Slice1(str string) (ret []int32) {
	strAry := StringToStrSlice1(str)
	for _, v := range strAry {
		ret = append(ret, Int32Value(v))
	}
	return ret
}

// 字符串转[]int32切片，分割符自定义
func StringToInt32SliceSplit(val string, split string) (ret []int32) {
	strAry := StringToStrSliceBySplit(val, split)
	for _, v := range strAry {
		ret = append(ret, Int32Value(v))
	}
	return ret
}

// 字符串转[]uint32切片，分割符","
func StringToUInt32Slice(val string) (ret []uint32) {
	strAry := StringToStrSlice(val)
	for _, v := range strAry {
		ret = append(ret, UInt32Value(v))
	}
	return ret
}

// 字符串转[]uint32切片，分割符"|"
func StringToUInt32Slice1(val string) (ret []uint32) {
	strAry := StringToStrSlice1(val)
	for _, v := range strAry {
		ret = append(ret, UInt32Value(v))
	}
	return ret
}

// 字符串转[]uint32切片，分割符自定义
func StringToUInt32SliceSplit(val string, split string) (ret []uint32) {
	strAry := StringToStrSliceBySplit(val, split)
	for _, v := range strAry {
		ret = append(ret, UInt32Value(v))
	}
	return ret
}

// 字符串转[]Int64切片，分割符","
func StringToInt64Slice(val string) (ret []int64) {
	strAry := StringToStrSlice(val)
	for _, v := range strAry {
		ret = append(ret, Int64Value(v))
	}
	return ret
}

// 字符串转[]Int64切片，分割符"|"
func StringToInt64Slice1(val string) (ret []int64) {
	strAry := StringToStrSlice1(val)
	for _, s := range strAry {
		ret = append(ret, Int64Value(s))
	}
	return ret
}

// 字符串转[]Int64切片，分割符自定义
func StringToInt64SliceSplit(val string, split string) (ret []int64) {
	strAry := StringToStrSliceBySplit(val, split)
	for _, s := range strAry {
		ret = append(ret, Int64Value(s))
	}
	return ret
}

// 字符串转[]uint32切片，分割符","
func StringToUInt64Slice(val string) (ret []uint64) {
	strAry := StringToStrSlice(val)
	for _, s := range strAry {
		ret = append(ret, UInt64Value(s))
	}
	return ret
}

// 字符串转[]uint32切片，分割符"|"
func StringToUInt64Slice1(val string) (ret []uint64) {
	strAry := StringToStrSlice1(val)
	for _, s := range strAry {
		ret = append(ret, UInt64Value(s))
	}
	return ret
}

// 字符串转[]float32切片，分割符","
func StringToFloat32Slice(val string) (ret []float32) {
	strAry := StringToStrSlice(val)
	for _, s := range strAry {
		ret = append(ret, Float32Value(s))
	}
	return ret
}

// 字符串转[]float32切片，分割符"|"
func StringToFloat32Slice1(val string) (ret []float32) {
	strAry := StringToStrSlice1(val)
	for _, s := range strAry {
		ret = append(ret, Float32Value(s))
	}
	return ret
}

// 字符串转[]float32切片，分割符自定义
func StringToFloat32SliceSplit(val string, split string) (ret []float32) {
	strAry := StringToStrSliceBySplit(val, split)
	for _, s := range strAry {
		ret = append(ret, Float32Value(s))
	}
	return ret
}

// 字符串转[]float64切片，分割符","
func StringToFloat64Slice(val string) (ret []float64) {
	strAry := StringToStrSlice(val)
	for _, s := range strAry {
		ret = append(ret, Float64Value(s))
	}
	return ret
}

// 字符串转[]float64切片，分割符"|"
func StringToFloat64Slice1(val string) (ret []float64) {
	strAry := StringToStrSlice1(val)
	for _, s := range strAry {
		ret = append(ret, Float64Value(s))
	}
	return ret
}

// 字符串转[]float64切片，分割符自定义
func StringToFloat64SliceSplit(val string, split string) (ret []float64) {
	strAry := StringToStrSliceBySplit(val, split)
	for _, s := range strAry {
		ret = append(ret, Float64Value(s))
	}
	return ret
}

// 字符串转map[uint64]uint32，分割符内层","，外层";"
func StringToMapUInt64Int(val string) (ret map[uint64]uint32) {
	strAry := StringToStrSliceBySplit(val, ";")
	var value []string
	for _, str := range strAry {
		if ret == nil {
			ret = make(map[uint64]uint32)
		}
		value = StringToStrSlice(str)
		ret[UInt64Value(value[0])] = UInt32Value(value[1])
	}
	return ret
}

// 字符串转map[uint64]uint32，分割符","
func MapUInt64IntToStr(m map[uint64]uint32) (str string) {
	str = ""
	if nil == m || len(m) == 0 {
		return str
	}
	for k, v := range m {
		str += fmt.Sprintf("%v,%v;", strconv.Itoa(int(k)), strconv.Itoa(int(v)))
	}

	return str[0 : len(str)-1]
}

// 截取字符串
// pos：裁切index
func SubString(s string, pos, length int) string {
	runes := []rune(s)
	l := pos + length
	if l > len(runes) {
		l = len(runes)
	}
	return string(runes[pos:l])
}

// buffer格式化
func BeautifulFormatByte(buf bytes.Buffer) (code []byte, err error) {
	code, err = format.Source(buf.Bytes())
	if err != nil {
		fmt.Println(buf.String())
		log.Fatalf("x 源字节流，无法格式化: %s", err)
	}

	code = bytes.Replace(code, []byte("\n\n"), []byte("\n"), -1)
	code = bytes.Replace(code, []byte("n = 0\n"), []byte("\n"), -1)
	code = bytes.Replace(code, []byte("+ 0\n"), []byte("\n"), -1)
	code, err = format.Source(code)
	if err != nil {
		fmt.Println(buf.String())
		log.Fatalf("x 优化字节流，无法格式化: %s", err)
	}
	return
}


// 转换为golang命名格式的名称
func ToGolangFileName(name string, sep rune) string {
	var a = []rune{}
	var last rune
	for _, r := range name {
		if last == '_' {
			if r == '_' {
				last = '\x00'
				continue
			} else {
				a[len(a)-1] = sep
			}
		}
		if last == '\x00' && r == '_' {
			continue
		}
		a = append(a, r)
		last = r
	}
	name = string(a)
	name = SnakeString(name)
	name = strings.Replace(name, "__", "_", -1)
	name = strings.Replace(name, string(sep)+"_", string(sep), -1)
	return name
}

func SnakeString(s string) string {
	data := make([]byte, 0, len(s)*2)
	j := false
	for _, d := range StringToBytes(s) {
		if d >= 'A' && d <= 'Z' {
			if j {
				data = append(data, '_')
				j = false
			}
		} else if d != '_' {
			j = true
		}
		data = append(data, d)
	}
	return strings.ToLower(BytesToString(data))
}

func StringToBytes(s string) []byte {
	sp := *(*[2]uintptr)(unsafe.Pointer(&s))
	bp := [3]uintptr{sp[0], sp[1], sp[1]}
	return *(*[]byte)(unsafe.Pointer(&bp))
}

func BytesToString(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}