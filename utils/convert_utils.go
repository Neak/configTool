/**
 *	Author: Neak
 *	Date: 2020/4/18
 *  Desc:
 */
package utils

import (
	"strconv"
	"strings"
)

func Int32ToStr(v int32) string {
	str := strconv.Itoa(int(v))
	return str
}
func UInt32ToStr(v uint32) string {
	str := strconv.Itoa(int(v))
	return str
}
func Int64ToStr(v int64) string {
	str := strconv.Itoa(int(v))
	return str
}
func UInt64ToStr(v uint64) string {
	str := strconv.Itoa(int(v))
	return str
}

func BoolValue(v string) bool {
	if v = strings.Trim(v, " "); v == "" {
		return false
	}
	b, _ := strconv.ParseBool(v)
	return b
}

func IntValue(v string) int {
	if v = strings.Trim(v, " "); v == "" {
		return 0
	}
	if i, err := strconv.ParseInt(v, 0, 32); err == nil {
		return int(i)
	} else {
		return 0
	}
}
func UIntValue(v string) uint {
	if v = strings.Trim(v, " "); v == "" {
		return 0
	}
	if i, err := strconv.ParseInt(v, 0, 32); err == nil {
		return uint(i)
	} else {
		return 0
	}
}

func Int32Value(v string) int32 {
	if v = strings.Trim(v, " "); v == "" {
		return 0
	}
	if i, err := strconv.ParseInt(v, 0, 32); err == nil {
		return int32(i)
	} else {
		return 0
	}
}
func UInt32Value(v string) uint32 {
	if v = strings.Trim(v, " "); v == "" {
		return 0
	}
	if i, err := strconv.ParseInt(v, 0, 32); err == nil {
		return uint32(i)
	} else {
		return 0
	}
}

func Int64Value(v string) int64 {
	if v = strings.Trim(v, " "); v == "" {
		return 0
	}
	if i, err := strconv.ParseInt(v, 0, 64); err == nil {
		return i
	} else {
		return 0
	}
}
func UInt64Value(v string) uint64 {
	if v = strings.Trim(v, " "); v == "" {
		return 0
	}
	if i, err := strconv.ParseInt(v, 0, 64); err == nil {
		return uint64(i)
	} else {
		return 0
	}
}

func Float32Value(v string) float32 {
	return float32(Float64Value(v))
}

func Float64Value(v string) float64 {
	if v = strings.Trim(v, " "); v == "" {
		return 0
	}
	index := strings.LastIndex(v, ".")
	if index == -1 {
		return float64(Int32Value(v))
	}
	if f, err := strconv.ParseFloat(v, len(v)-1-index); err == nil {
		return f
	} else {
		return 0
	}
}
