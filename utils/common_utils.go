/**
 *	Author: Neak
 *	Date: 2020/4/20
 *  Desc:
 */
package utils

//判定iVal是否存在于iList
func StrListContains(strList []string, val string) bool {
	for _, v := range strList {
		if v == val {
			return true
		}
	}
	return false
}

//判定iVal是否存在于iList
func ListContains(strList []interface{}, val interface{}) bool {
	for _, v := range strList {
		if v == val {
			return true
		}
	}
	return false
}
