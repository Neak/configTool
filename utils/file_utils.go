/**
 *	Author: Neak
 *	Date: 2020/4/20
 *  Desc:
 */
package utils

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"runtime"
	"strings"
)

// 清空目录
func RemoveContents(dir string) error {
	d, err := os.Open(dir)
	if err != nil {
		return err
	}
	defer d.Close()
	names, err := d.Readdirnames(-1)
	if err != nil {
		return err
	}
	for _, name := range names {
		err = os.RemoveAll(filepath.Join(dir, name))
		if err != nil {
			return err
		}
	}
	return nil
}

// 判断文件是否存在，不存在则创建
func PathExists(path string) {
	_, err := os.Stat(path)
	if os.IsNotExist(err) {
		os.Create(path)
	}
}

// 判断目录是否存在，不存在则创建
func DirExists(path string) {
	_, err := os.Stat(path)
	if os.IsNotExist(err) {
		os.Mkdir(path, os.ModePerm)
	}
}

func GetTemplate(filePath string) string {
	b, err := ioutil.ReadFile(filePath)
	if err != nil {
		panic(fmt.Sprintf("read file: %v error: %v", filePath, err))
	}
	s := string(b)
	for _, lineStr := range strings.Split(s, "\n") {
		lineStr = strings.TrimSpace(lineStr)
		if lineStr == "" {
			continue
		}
	}
	return s
}

func getCurPath() string {
	_, path, _, ok := runtime.Caller(1)
	if !ok {
		panic(errors.New("Can not get current file info"))
	}

	return path
}
